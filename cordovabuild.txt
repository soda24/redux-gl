nar@ubuntu:/var/development/granliga/redux-gl$ cordova create cordova-dist com.granliga.observer GranLiga
Creating a new cordova project.
nar@ubuntu:/var/development/granliga/redux-gl$ cd cordova-dist
nar@ubuntu:/var/development/granliga/redux-gl/cordova-dist$ cordova platform add android
Adding android project...
Creating Cordova project for the Android platform:
        Path: platforms/android
        Package: com.granliga.observer
        Name: GranLiga
        Activity: MainActivity
        Android target: android-22
Copying template files...
Android project created with cordova-android@4.1.1
Discovered plugin "cordova-plugin-whitelist" in config.xml. Installing to the project
Fetching plugin "cordova-plugin-whitelist@1" via npm
Installing "cordova-plugin-whitelist" for android

This plugin is only applicable for versions of cordova-android greater than 4.0. If you have a previous platform version, you do *not* need this plugin since the whitelist will be built in.

cordova build android --release

nar@ubuntu:/var/development/granliga/redux-gl/cordova-dist$ keytool -genkey -v -keystore ~/.android/GranLiga.keystore -alias GranLiga -keyalg RSA -keysize 2048 -validity 10000
nar@ubuntu:/var/development/granliga/redux-gl/cordova-dist$ export DISPLAY=192.168.0.4:0.0
nar@ubuntu:/var/development/granliga/redux-gl/cordova-dist$ export PATH=/opt/jdk/jdk1.8.0_60/bin:$PATH
nar@ubuntu:/var/development/granliga/redux-gl/cordova-dist$ cordova build android --release
Nicolass-Mac-mini:cordova-dist nicolas$ adb install /Users/nicolas/development/mobile/redux-gl/cordova-dist/platforms/android/build/outputs/apk/android-release.apk
