export default function cacheMiddleware(db) {
  return ({dispatch, getState}) => {
    return next => action => {
      if (typeof action === 'function') {
        return action(dispatch, getState);
      }

      const { cache, types, ...rest } = action;
      if (!cache) {
        return next(action);
      }

      const [REQUEST, SUCCESS, FAILURE] = types;
      next({...rest, type: REQUEST});
      return cache(db).then(
        (result) => next({...rest, result, type: SUCCESS}),
        (error) => {
          next({...rest, error, type: FAILURE}); throw error
        }
      ).catch((error)=> {
        console.error('MIDDLEWARE ERROR:', error);
        next({...rest, error, type: FAILURE});
      });
    };
  };
}