import { combineReducers } from 'redux';
import { routerStateReducer as router } from 'redux-router';

import matches from './matches';
import events from './events';
import tournaments from './tournaments';
import auth from './auth';
import system from './system';
import matchPlayers from './matchPlayers';
import temporalEvent from './temporalEvent';

export default combineReducers({
  router,
  matches,
  events,
  tournaments,
  auth,
  system,
  matchPlayers,
  temporalEvent,
});
