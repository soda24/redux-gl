import {
  MATCH_PLAYER_REMOTE_LOAD,
  MATCH_PLAYER_REMOTE_LOAD_SUCCESS,
  MATCH_PLAYER_REMOTE_LOAD_FAIL,
  MATCH_PLAYER_CACHE_LOAD,
  MATCH_PLAYER_CACHE_LOAD_SUCCESS,
  MATCH_PLAYER_CACHE_LOAD_FAIL,
} from '../../actions/actionTypes';

import moment from 'moment';

const initialState = {
  loaded: false
};

export default function matchPlayers(state = initialState, action = {}) {
  switch (action.type) {
    case MATCH_PLAYER_CACHE_LOAD:
      return {
        ...state,
        cacheLoading: true
      };
    case MATCH_PLAYER_CACHE_LOAD_SUCCESS:
      const expiresAt = action.result.data && _.min(action.result.data, function(r) { return r.expiresAt}).expiresAt;
      return {
        ...state,
        cacheLoading: false,
        cacheLoaded: true,
        data: action.result && action.result.data,
        cacheExpirationDate: expiresAt,
        cacheError: null
      };
    case MATCH_PLAYER_CACHE_LOAD_FAIL:
      return {
        ...state,
        cacheLoading: false,
        cacheLoaded: false,
        data: null,
        cacheError: action.error
      };
    case MATCH_PLAYER_CACHE_LOAD:
      return {
        ...state,
        cacheLoading: true
      };
    case MATCH_PLAYER_REMOTE_LOAD_SUCCESS:
      return {
        ...state,
        remoteLoading: false,
        remoteLoaded: true,
        remoteError: null
      };
    case MATCH_PLAYER_REMOTE_LOAD_FAIL:
      return {
        ...state,
        remoteLoading: false,
        remoteLoaded: false,
        data: null,
        remoteError: action.error
      };
    default:
      return state;
  }
}

export function filterByLocal(globalState, matchId, local) {
  if (globalState.matchPlayers && globalState.matchPlayers.data && _.keys(globalState.matchPlayers.data).length) {
    return globalState.matchPlayers.data.filter(function(mp){return mp.local === local})
  }
  return []
}