import {
  AUTH_LOAD,
  AUTH_LOAD_SUCCESS,
  AUTH_LOAD_FAIL,
  AUTH_LOGIN,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAIL,
  AUTH_CACHE_DELETE,
  AUTH_CACHE_DELETE_SUCCESS,
  AUTH_CACHE_DELETE_FAIL,
  AUTH_LOGOUT,
  AUTH_LOGOUT_SUCCESS,
  AUTH_LOGOUT_FAIL,
  AUTH_TOKEN_EXPIRED,
} from '../../actions/actionTypes';

const initialState = {
  loaded: false
};

export default function info(state = initialState, action = {}) {
  switch (action.type) {
    case AUTH_TOKEN_EXPIRED:
      return {
        ...state,
        token: null
      }
    case AUTH_LOAD:
      return {
        ...state,
        loading: true
      };
    case AUTH_LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        user: action.result.user,
        token: action.result.token,
      };
    case AUTH_LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case AUTH_LOGIN:
      return {
        ...state,
        loggingIn: true
      };
    case AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        loggingIn: false,
        user: action.result.user,
        token: action.result.token
      };
    case AUTH_LOGIN_FAIL:
      return {
        ...state,
        loggingIn: false,
        user: null,
        loginError: action.error
      };
    case AUTH_LOGOUT:
    case AUTH_CACHE_DELETE:
      return {
        ...state,
        loggingOut: true
      };
    case AUTH_LOGOUT_SUCCESS:
    case AUTH_CACHE_DELETE_SUCCESS:
      return {
        ...state,
        loggingOut: false,
        user: null
      };
    case AUTH_LOGOUT_FAIL: 
    case AUTH_CACHE_DELETE_FAIL:
      return {
        ...state,
        loggingOut: false,
        logoutError: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.auth && globalState.auth.loaded;
}

export function isAuthRequired(globalState) {
  return globalState.auth && globalState.auth.isAuthRequired;
}

export function getToken(globalState) {
  return globalState.auth && globalState.auth.token;
}

export function isAboutToLogIn(globalState) {
  return globalState.auth && (globalState.auth.loggingIn || globalState.auth.loading)
}

// export function isAuthenticated(globalState) {
//   return globalState.auth && globalState.auth.user && globalState.auth.token;
// }