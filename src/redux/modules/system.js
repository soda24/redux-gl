import {
  SYSTEM_ONLINE,
} from '../../actions/actionTypes';

const initialState = {
  online: true
};

export default function system(state = initialState, action = {}) {
  switch (action.type) {
    case SYSTEM_ONLINE:
      return {
        ...state,
        online: action.result
      }
    default:
      return state;
  }
}

export function isOnline(globalState) {
  return globalState.system && globalState.system.online;
}

export function isOffline(globalState) {
  return globalState.system && !globalState.system.online;
}
