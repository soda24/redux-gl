import {
  MATCH_REMOTE_LOAD,
  MATCH_REMOTE_LOAD_SUCCESS,
  MATCH_REMOTE_LOAD_FAIL,
  MATCH_CACHE_LOAD,
  MATCH_CACHE_LOAD_SUCCESS,
  MATCH_CACHE_LOAD_FAIL,
} from '../../actions/actionTypes';

import moment from 'moment';
import _ from 'lodash';

const initialState = {
  loaded: false
};


export default function matches(state = initialState, action = {}) {
  switch (action.type) {
    case MATCH_CACHE_LOAD:
      return {
        ...state,
        cacheLoading: true
      };
    case MATCH_CACHE_LOAD_SUCCESS:
      let data = action.result && action.result.data;
      let expiresAt = data && _.min(action.result.data, function(r) { return r.expiresAt}).expiresAt
      return {
        ...state,
        cacheLoading: false,
        cacheLoaded: true,
        data: _.indexBy(data, 'id'),
        cacheExpirationDate: expiresAt,
        cacheError: null
      };
    case MATCH_CACHE_LOAD_FAIL:
      return {
        ...state,
        cacheLoading: false,
        cacheLoaded: false,
        data: null,
        cacheError: action.error
      };
    case MATCH_CACHE_LOAD:
      return {
        ...state,
        cacheLoading: true
      };
    case MATCH_REMOTE_LOAD_SUCCESS:
      return {
        ...state,
        remoteLoading: false,
        remoteLoaded: true,
        remoteError: null
      };
    case MATCH_REMOTE_LOAD_FAIL:
      return {
        ...state,
        remoteLoading: false,
        remoteLoaded: false,
        remoteError: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.matches && globalState.matches.loaded;
}

export function isRemoteLoaded(globalState) {
  return globalState.matches && globalState.matches.remoteLoaded;
}

export function isRemoteNeeded(globalState) {
  let matches = globalState.matches && globalState.matches;
  //Ms were retrieved from the cache but the cache is empty
  if (matches.cacheLoaded && (!matches.data || !_.keys(matches.data).length)) return true;

  //The cache has no expiration date or is expired
  let exp = matches && matches.cacheExpirationDate;
  if (!exp || moment(exp, 'YYYYMMDDhhmmdd') < moment()) return true;

  //No remote syncing is needed
  return false
}

export function getAll(globalState) {
  return _.values(globalState.matches && globalState.matches.data || {})
}

export function getById(globalState, matchId) {
  if (globalState.matches && globalState.matches.data) {
    return _.find(globalState.matches.data, function(m) {return m.id === parseInt(matchId)})
  }
}

export function listPendingByTournament(globalState) {
  if (globalState.matches && globalState.matches.data && _.keys(globalState.matches.data).length) {
    let ids = []
    _.forOwn(globalState.matches.data, function(m){ ids.push(m.tournament_id)})
    return _.countBy(ids) || {}
  }
  return []
}