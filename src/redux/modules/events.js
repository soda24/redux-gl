import {
  EVENT_REMOTE_LOAD,
  EVENT_REMOTE_LOAD_SUCCESS,
  EVENT_REMOTE_LOAD_FAIL,
  EVENT_CACHE_LOAD,
  EVENT_CACHE_LOAD_SUCCESS,
  EVENT_CACHE_LOAD_FAIL,
  EVENT_ADD,
  EVENT_REMOVE,
  EVENT_CLEAR,
  EVENT_CACHE_DELETE_SUCCESS,
} from '../../actions/actionTypes';

import moment from 'moment';
import _ from 'lodash';

const initialState = {
  loaded: false
};


export default function events(state = initialState, action = {}) {
  switch (action.type) {
    case EVENT_CACHE_LOAD:
      return {
        ...state,
        cacheLoading: true
      };
    case EVENT_CACHE_LOAD_SUCCESS:
      const data = action.result && action.result.data;
      const expiresAt = action.result.data && _.min(action.result.data, (r) =>  { return r.expiresAt }).expiresAt;
      return {
        ...state,
        cacheLoading: false,
        cacheLoaded: true,
        data: action.result && action.result.data,
        cacheExpirationDate: expiresAt,
        cacheError: null
      };
    case EVENT_CACHE_LOAD_FAIL:
      return {
        ...state,
        cacheLoading: false,
        cacheLoaded: false,
        data: null,
        cacheError: action.error
      };
    case EVENT_CACHE_LOAD:
      return {
        ...state,
        cacheLoading: true
      };
    case EVENT_REMOTE_LOAD_SUCCESS:
      return {
        ...state,
        remoteLoading: false,
        remoteLoaded: true,
        remoteError: null
      };
    case EVENT_REMOTE_LOAD_FAIL:
      return {
        ...state,
        remoteLoading: false,
        remoteLoaded: false,
        remoteError: action.error
      };
    case EVENT_CLEAR:
      return {
        ...state,
        data: {},
      };
    case EVENT_ADD:
      // let id = action.result.id
      // let d = {}
      // d[id] = action.result
      return {
        ...state,
        data: {...state.data, ...{[action.result.id]: action.result}}
      }
    case EVENT_REMOVE:
      let d = {...state.data};
      console.log(_.keys(d).length);
      console.log(action.id, d[action.id], d);
      delete d[action.id];
      console.log(_.keys(d).length);
      console.log('-----------------');
      let evArr = _.map(d, (ev, idx) => {
        return [idx, ev];
      });
      evArr = _.filter(evArr, (obj) => {
        return obj[1].id !== action.id;
      });
      console.log(evArr.length);
      console.log(_.fromPairs(evArr).length);
      return {
        ...state,
        data: d
      }
    case EVENT_CACHE_DELETE_SUCCESS:
      const d2 = {...state.data};
      delete d2[action.id];
      return {
        ...state,
        data: d2
      }
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.events && globalState.events.loaded;
}

export function isRemoteLoaded(globalState) {
  return globalState.events && globalState.events.remoteLoaded;
}

export function isRemoteNeeded(globalState) {
  let events = globalState.events && globalState.events;
  //Ms were retrieved from the cache but the cache is empty
  if (events.cacheLoaded && (!events.data || !_.keys(events.data).length)) return true;

  //The cache has no expiration date or is expired
  const exp = events && events.cacheExpirationDate;
  if (!exp || moment(exp, 'YYYYMMDDhhmmdd') < moment()) return true;

  //No remote syncing is needed
  return false
}

export function getAll(globalState) {
  return _.values(globalState.events && globalState.events.data || {})
}

export function getMatchEvents(globalState, matchId) {
  if (globalState.events && globalState.events.data) {
    return _.filter(_.values(globalState.events.data), function(e) {return parseInt(e.match_id) === parseInt(matchId)})
  }
}

export function getById(globalState, eventId) {
  if (globalState.events && globalState.events.data) {
    return _.find(globalState.events.data, function(e) {return e.id === parseInt(eventId)})
  }
}

export function getByType(globalState, matchId, typeId, teamId) {
  if (globalState.events && globalState.events.data) {
    let evts = _.filter(_.values(globalState.events.data), function(e) {
      return parseInt(e.match_id) === parseInt(matchId) && e.event_type == typeId
    }) || []
    if (teamId) {
      evts = evts.filter(function(ev){
        return ev.team_id == teamId
      })
    }
    return evts
  }
}