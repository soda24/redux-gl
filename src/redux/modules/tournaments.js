import {
  TOURNAMENT_REMOTE_LOAD,
  TOURNAMENT_REMOTE_LOAD_SUCCESS,
  TOURNAMENT_REMOTE_LOAD_FAIL,
  TOURNAMENT_CACHE_LOAD,
  TOURNAMENT_CACHE_LOAD_SUCCESS,
  TOURNAMENT_CACHE_LOAD_FAIL,
} from '../../actions/actionTypes';

import moment from 'moment';
import _ from 'lodash';

const initialState = {
  loaded: false
};


export default function tournaments(state = initialState, action = {}) {
  switch (action.type) {
    case TOURNAMENT_CACHE_LOAD:
      return {
        ...state,
        cacheLoading: true
      };
    case TOURNAMENT_CACHE_LOAD_SUCCESS:
      let data = action.result && action.result.data;
      let expiresAt = data && _.min(data, function(r) { return r.expiresAt}).expiresAt
      return {
        ...state,
        cacheLoading: false,
        cacheLoaded: true,
        data: _.indexBy(data, 'id'),
        cacheExpirationDate: expiresAt,
        cacheError: null
      };
    case TOURNAMENT_CACHE_LOAD_FAIL:
      return {
        ...state,
        cacheLoading: false,
        cacheLoaded: false,
        data: null,
        cacheError: action.error
      };
    case TOURNAMENT_CACHE_LOAD:
      return {
        ...state,
        cacheLoading: true
      };
    case TOURNAMENT_REMOTE_LOAD_SUCCESS:
      return {
        ...state,
        remoteLoading: false,
        remoteLoaded: true,
        remoteError: null
      };
    case TOURNAMENT_REMOTE_LOAD_FAIL:
      return {
        ...state,
        remoteLoading: false,
        remoteLoaded: false,
        remoteError: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.tournaments && globalState.tournaments.loaded;
}

export function isRemoteLoaded(globalState) {
  return globalState.tournaments && globalState.tournaments.remoteLoaded;
}

export function isRemoteNeeded(globalState) {
  let tournaments = globalState.tournaments && globalState.tournaments;
  //Ms were retrieved from the cache but the cache is empty
  if (tournaments.cacheLoaded && (!tournaments.data || !_.keys(tournaments.data).length)) return true;

  //The cache has no expiration date or is expired
  let exp = tournaments && tournaments.cacheExpirationDate;
  if (!exp || moment(exp, 'YYYYMMDDhhmmdd') < moment()) return true;

  //No remote syncing is needed
  return false;
}

export function getAll(globalState) {
  return _.values(globalState.tournaments && globalState.tournaments.data || {})
}

export function getById(globalState, tournamentId) {
  if (globalState.tournaments && globalState.tournaments.data) {
    return _.find(globalState.tournaments.data, function(m) {return m.id === parseInt(tournamentId)}) || {}
  }
}
