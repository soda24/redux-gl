import {
  TEMPORAL_EVENT_RESET_PLAYER2,
  TEMPORAL_EVENT_RESET_PLAYER,
  TEMPORAL_EVENT_RESET_TEAM,
  TEMPORAL_EVENT_RESET_ALL,
  TEMPORAL_EVENT_RESET_TIME,
  TEMPORAL_EVENT_RESET_HALF_TIME,
  TEMPORAL_EVENT_SET_HALF_TIME,
  TEMPORAL_EVENT_SET_TIME,
  TEMPORAL_EVENT_SET_EVENT_TYPE,
  TEMPORAL_EVENT_SET_TEAM_ID,
  TEMPORAL_EVENT_SET_PROFILE_ID,
  TEMPORAL_EVENT_SET_PROFILE2_ID,
} from '../../actions/actionTypes';

import moment from 'moment';
import _ from 'lodash';

const initialState = {
  half_time: null,
  last_half_time: null,
  last_time: 1,
  time: null,
  type: null,
  team_id: null,
  profile_id: null,
  profile2_id: null,
  position: null,
  position2: null
}

export default function temporalEvent(state = initialState, action = {}) {
  switch (action.type) {
    case TEMPORAL_EVENT_RESET_PLAYER2:
      return {
        ...state,
        profile2_id: null,
        position2: null,
      }
    case TEMPORAL_EVENT_RESET_PLAYER:
      return {
        ...state,
        profile_id: null,
        profile2_id: null,
        position: null,
        position2: null,
      }
    case TEMPORAL_EVENT_RESET_TEAM:
      return {
        ...state,
        team_id: null,
        profile_id: null,
        profile2_id: null,
        position: null,
        position2: null,
      }
    case TEMPORAL_EVENT_RESET_ALL:
      return {
        ...initialState,
      }
    case TEMPORAL_EVENT_RESET_TIME:
      return {
        ...state,
        type: null,
        time: null,
        last_time: null,
        team_id: null,
        profile_id: null,
        profile2_id: null,
        position: null,
        position2: null,
      }
    case TEMPORAL_EVENT_RESET_HALF_TIME:
      return {
        ...state,
        type: null,
        time: null,
        half_time: null,
        last_time: null,
        team_id: null,
        profile_id: null,
        profile2_id: null,
        position: null,
        position2: null,
      }
    case TEMPORAL_EVENT_SET_HALF_TIME:
      return {
        ...state,
        half_time: parseInt(action.halfTime),
        last_half_time: parseInt(action.halfTime),
      }
    case TEMPORAL_EVENT_SET_TIME:
      return {
        ...state,
        time: parseInt(action.time),
        last_time: parseInt(action.time),
      }
    case TEMPORAL_EVENT_SET_EVENT_TYPE:
      return {
        ...state,
        type: action.eventType,
      }
    case TEMPORAL_EVENT_SET_TEAM_ID:
      return {
        ...state,
        team_id: action.teamId,
      }
    case TEMPORAL_EVENT_SET_PROFILE_ID:
      return {
        ...state,
        profile_id: action.profile_id,
        position: action.position,
      }
    case TEMPORAL_EVENT_SET_PROFILE2_ID:
      return {
        ...state,
        profile2_id: action.profile_id,
        position2: action.position,
      }
   default:
      return state;
  }
}