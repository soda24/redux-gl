Pasos para saber si la información fue obtenida y es confiable:

cacheLoading
cacheLoaded
cacheError
remoteLoading
remoteLoaded
remoteError


1 cacheLoading?
  N:
    cacheLoaded?
      N: "Cargando información (mensaje de waiting)"
      Y:
      	informationExists?
      	  N:
      	  	cacheError?
      	  	  N: "No puede ser ya que el no encontrar un dato lo considera como un error creo, ver esto"
      	  	  Y:
      	        remoteLoaded?
      	          N:
      	            remoteLoading?
      	              N:
      	                offline?
      	                  N: `Fire Action for remote syncing`
      	                  Y: `Mensaje necesita internet para sincronizar con el servidor`
      	              Y: "Sincronizando con el servidor"
      	  Y:
            isInformationReliable?
              Y: Proceed (END)
              N:
                remoteLoading?
                  N:
                    offline?
                      N:
                        remoteLoaded?
	                      N: `Fire Action for remote syncing`
	                      Y:
	                        remoteError:
	                          Posible conflicto con la información del servidor, habría que considerar limpiar el cache, esto se desprende de los distintos tipos de error que vengan del servidor.
	                  Y: `Mensaje necesita internet para sincronizar con el servidor, la información que tiene es vieja y puede ocasionar problemas si procede a actualizarla`
                  Y: "Mensaje de retrieving information from the server"
