import {
  EVENT_REMOTE_LOAD,
  EVENT_REMOTE_LOAD_SUCCESS,
  EVENT_REMOTE_LOAD_FAIL,
  EVENT_REMOTE_SAVE,
  EVENT_REMOTE_SAVE_SUCCESS,
  EVENT_REMOTE_SAVE_FAIL,
  EVENT_ADD,
  EVENT_REMOVE,
  EVENT_CLEAR,
  EVENT_CACHE_LOAD,
  EVENT_CACHE_LOAD_SUCCESS,
  EVENT_CACHE_LOAD_FAIL,
  EVENT_CACHE_STORE,
  EVENT_CACHE_STORE_SUCCESS,
  EVENT_CACHE_STORE_FAIL,
  EVENT_CACHE_DELETE,
  EVENT_CACHE_DELETE_SUCCESS,
  EVENT_CACHE_DELETE_FAIL,
} from './actionTypes';

import moment from 'moment';

export function loadRemote(token, matchId, filters={}) {
  return {
    types: [EVENT_REMOTE_LOAD, EVENT_REMOTE_LOAD_SUCCESS, EVENT_REMOTE_LOAD_FAIL],
    promise: (client) => client.get('/tournamentmanagement/matches/'+matchId+'/events', {...filters, token: token}),
  };
}

export function saveRemote(token, matchId, data, filters={}) {
  return {
    types: [EVENT_REMOTE_SAVE, EVENT_REMOTE_SAVE_SUCCESS, EVENT_REMOTE_SAVE_FAIL],
    promise: (client) => client.put('/tournamentmanagement/matches/'+matchId+'/events', {...filters, token: token, data}),
  };
}

export function loadCache(key) {
  return {
    types: [EVENT_CACHE_LOAD, EVENT_CACHE_LOAD_SUCCESS, EVENT_CACHE_LOAD_FAIL],
    cache: (db) => db._list(key)
  };
}

export function storeCache(key, data) {
  const expirationDate = moment().add(2, 'days').format('YYYYMMDDhhmmss');
  return {
    types: [EVENT_CACHE_STORE, EVENT_CACHE_STORE_SUCCESS, EVENT_CACHE_STORE_FAIL],
    cache: (db) => db.put(key, data, expirationDate)
  };
}

export function deleteCache(key, isArr=false) {
  return {
    types: [EVENT_CACHE_DELETE, EVENT_CACHE_DELETE_SUCCESS, EVENT_CACHE_DELETE_FAIL],
    cache: (db) => db.delete(key, isArr)
  };
}

export function add(event) {
  return {
    type: EVENT_ADD,
    result: event
  }
}

export function remove(eventId) {
  return {
    type: EVENT_REMOVE,
    id: eventId
  }
}

export function clear() {
  return {
    type: EVENT_CLEAR
  }
}

export function retrieveCache(key) {
  return {
    types: ['EVENT_CACHE_LIST', 'EVENT_CACHE_LIST_SUCCESS', 'EVENT_CACHE_FAIL'],
    cache: (db) => db._list(key)
  };
}
