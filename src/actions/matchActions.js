import {
  MATCH_REMOTE_LOAD,
  MATCH_REMOTE_LOAD_SUCCESS,
  MATCH_REMOTE_LOAD_FAIL,
  MATCH_CACHE_LOAD,
  MATCH_CACHE_LOAD_SUCCESS,
  MATCH_CACHE_LOAD_FAIL,
  MATCH_CACHE_STORE,
  MATCH_CACHE_STORE_SUCCESS,
  MATCH_CACHE_STORE_FAIL,
} from './actionTypes';

import moment from 'moment';

export function loadRemote(token) {
  return {
    types: [MATCH_REMOTE_LOAD, MATCH_REMOTE_LOAD_SUCCESS, MATCH_REMOTE_LOAD_FAIL],
    promise: (client) => client.get('/tournamentmanagement/matches', {params: {observation_status: 'p'}, token: token}),
  };
}

export function loadCache(key) {
  return {
    types: [MATCH_CACHE_LOAD, MATCH_CACHE_LOAD_SUCCESS, MATCH_CACHE_LOAD_FAIL],
    cache: (db) => db._list(key)
  };
}

export function storeCache(key, data) {
  let expirationDate = moment().add(2, 'days').format('YYYYMMDDhhmmss')
  return {
    types: [MATCH_CACHE_STORE, MATCH_CACHE_STORE_SUCCESS, MATCH_CACHE_STORE_FAIL],
    cache: (db) => db.put(key, data, expirationDate)
  };
}
