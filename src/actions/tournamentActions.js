import {
  TOURNAMENT_REMOTE_LOAD,
  TOURNAMENT_REMOTE_LOAD_SUCCESS,
  TOURNAMENT_REMOTE_LOAD_FAIL,
  TOURNAMENT_CACHE_LOAD,
  TOURNAMENT_CACHE_LOAD_SUCCESS,
  TOURNAMENT_CACHE_LOAD_FAIL,
  TOURNAMENT_CACHE_STORE,
  TOURNAMENT_CACHE_STORE_SUCCESS,
  TOURNAMENT_CACHE_STORE_FAIL,
} from './actionTypes';

import moment from 'moment';

export function loadRemote(token) {
  return {
    types: [TOURNAMENT_REMOTE_LOAD, TOURNAMENT_REMOTE_LOAD_SUCCESS, TOURNAMENT_REMOTE_LOAD_FAIL],
    promise: (client) => client.get('/tournamentmanagement/tournaments', {token: token}),
  };
}

export function loadCache(key) {
  return {
    types: [TOURNAMENT_CACHE_LOAD, TOURNAMENT_CACHE_LOAD_SUCCESS, TOURNAMENT_CACHE_LOAD_FAIL],
    cache: (db) => db._list(key)
  };
}

export function storeCache(key, data) {
  const expirationDate = moment().add(2, 'days').format('YYYYMMDDhhmmss');
  // console.log('expirationDate', expirationDate);
  return {
    types: [TOURNAMENT_CACHE_STORE, TOURNAMENT_CACHE_STORE_SUCCESS, TOURNAMENT_CACHE_STORE_FAIL],
    cache: (db) => db.put(key, data, expirationDate)
  };
}
