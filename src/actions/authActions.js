import {
  AUTH_LOAD,
  AUTH_LOAD_SUCCESS,
  AUTH_LOAD_FAIL,
  AUTH_STORE,
  AUTH_STORE_SUCCESS,
  AUTH_STORE_FAIL,
  AUTH_CACHE_DELETE,
  AUTH_CACHE_DELETE_SUCCESS,
  AUTH_CACHE_DELETE_FAIL,
  AUTH_LOGIN,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAIL,
  AUTH_LOGOUT,
  AUTH_LOGOUT_SUCCESS,
  AUTH_LOGOUT_FAIL,
  USER_STORE,
  USER_STORE_SUCCESS,
  USER_STORE_FAIL,

} from './actionTypes';

export function storeAccount(data) {
  console.log('storeAccount', data);
  return {
    types: [AUTH_STORE, AUTH_STORE_SUCCESS, AUTH_STORE_FAIL],
    cache: (db) => db.put('auth', data)
  };
}

export function loadAccount(data) {
  return {
    types: ['NONE', 'NONE', 'DUMMY'],
    cache: (db) => db._get('auth')
  };
}

export function storeMe(data) {
  return {
    types: [USER_STORE, USER_STORE_SUCCESS, USER_STORE_FAIL],
    cache: (db) => db.put('me', data)
  };
}

export function loadMe() {
  return {
    types: [AUTH_LOAD, AUTH_LOAD_SUCCESS, AUTH_LOAD_FAIL],
    cache: (db) => db._get('me')
  };
}

export function login(username, password, role="observer") {
  return {
    types: [AUTH_LOGIN, AUTH_LOGIN_SUCCESS, AUTH_LOGIN_FAIL],
    promise: (client) => client.post('/users/login', {
      data: {
        username: username,
        password: password,
        role: role,
      }
    })
  };
}

export function logout() {
  return {
    types: [AUTH_LOGOUT, AUTH_LOGOUT_SUCCESS, AUTH_LOGOUT_FAIL],
    promise: (client) => client.get('/users/logout')
  };
}

export function deleteCache(key, isArr=false) {
  return {
    types: [AUTH_CACHE_DELETE, AUTH_CACHE_DELETE_SUCCESS, AUTH_CACHE_DELETE_FAIL,],
    cache: (db) => db.delete(key, isArr)
  };
}
