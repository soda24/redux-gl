import {
  SYSTEM_ONLINE,
} from './actionTypes';

export function setOnline(ONLINE_STATUS) {
  return {
    type: SYSTEM_ONLINE,
    result: ONLINE_STATUS
  };
}