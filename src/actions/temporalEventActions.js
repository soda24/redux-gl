import {
  TEMPORAL_EVENT_RESET_PLAYER2,
  TEMPORAL_EVENT_RESET_PLAYER,
  TEMPORAL_EVENT_RESET_TEAM,
  TEMPORAL_EVENT_RESET_ALL,
  TEMPORAL_EVENT_RESET_TIME,
  TEMPORAL_EVENT_RESET_HALF_TIME,
  TEMPORAL_EVENT_SET_HALF_TIME,
  TEMPORAL_EVENT_SET_TIME,
  TEMPORAL_EVENT_SET_EVENT_TYPE,
  TEMPORAL_EVENT_SET_TEAM_ID,
  TEMPORAL_EVENT_SET_PROFILE_ID,
  TEMPORAL_EVENT_SET_PROFILE2_ID,
} from './actionTypes';

import moment from 'moment';

export function resetPlayer2() {
  return {
    type: TEMPORAL_EVENT_RESET_PLAYER2,
  }
}

export function resetPlayer() {
  return {
    type: TEMPORAL_EVENT_RESET_PLAYER,
  }
}

export function resetTeam() {
  return {
    type: TEMPORAL_EVENT_RESET_TEAM,
  }
}

export function resetEvent() {
  return {
    type: TEMPORAL_EVENT_RESET_ALL,
  }
}

export function resetTime() {
  return {
    type: TEMPORAL_EVENT_RESET_TIME,
  }
}

export function resetHalfTime() {
  return {
    type: TEMPORAL_EVENT_RESET_HALF_TIME,
  }
}

export function setHalfTime(halfTime) {
  return {
    type: TEMPORAL_EVENT_SET_HALF_TIME,
    halfTime
  }
}

export function setTime(time) {
  return {
    type: TEMPORAL_EVENT_SET_TIME,
    time
  }
}

export function setEventType(eventType) {
  return {
    type: TEMPORAL_EVENT_SET_EVENT_TYPE,
    eventType
  }
}

export function setTeam(teamId) {
  return {
    type: TEMPORAL_EVENT_SET_TEAM_ID,
    teamId
  }
}

export function setPlayer(profileId, position) {
  return {
    type: TEMPORAL_EVENT_SET_PROFILE_ID,
    profile_id: profileId,
    null
  }
}

export function setPlayer2(profileId, position) {
  return {
    type: TEMPORAL_EVENT_SET_PROFILE2_ID,
    profile2_id: profileId,
    position2: position
  }
}