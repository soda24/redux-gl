import {
  MATCH_PLAYER_CACHE_LOAD,
  MATCH_PLAYER_CACHE_LOAD_SUCCESS,
  MATCH_PLAYER_CACHE_LOAD_FAIL,
  MATCH_PLAYER_REMOTE_LOAD,
  MATCH_PLAYER_REMOTE_LOAD_SUCCESS,
  MATCH_PLAYER_REMOTE_LOAD_FAIL,
  MATCH_PLAYER_CACHE_STORE,
  MATCH_PLAYER_CACHE_STORE_SUCCESS,
  MATCH_PLAYER_CACHE_STORE_FAIL,
  MATCH_PLAYER_CACHE_DELETE,
  MATCH_PLAYER_CACHE_DELETE_SUCCESS,
  MATCH_PLAYER_CACHE_DELETE_FAIL
} from './actionTypes';

import moment from 'moment';

export function loadRemote(token, match_id, team) {
  return {
    types: [MATCH_PLAYER_REMOTE_LOAD, MATCH_PLAYER_REMOTE_LOAD_SUCCESS, MATCH_PLAYER_REMOTE_LOAD_FAIL,
            // (data) => storeCache.bind(true, store_cache_key)({data: data, expiresAt: expirationDate})
           ],
    promise: (client) => client.get('/tournamentmanagement/matches/'+match_id+'/players', {token: token}),
  };
}

export function loadCache(key) {
  return {
    types: [MATCH_PLAYER_CACHE_LOAD, MATCH_PLAYER_CACHE_LOAD_SUCCESS, MATCH_PLAYER_CACHE_LOAD_FAIL],
    cache: (db) => db._list(key)
  };
}

export function storeCache(key, data) {
  const expirationDate = moment().add(2, 'days').format('YYYYMMDDhhmmss');
  return {
    types: [MATCH_PLAYER_CACHE_STORE, MATCH_PLAYER_CACHE_STORE_SUCCESS, MATCH_PLAYER_CACHE_STORE_FAIL],
    cache: (db) => db.put(key, data, expirationDate)
  };
}

export function deleteCache(key, isArr=false) {
  return {
    types: [MATCH_PLAYER_CACHE_DELETE, MATCH_PLAYER_CACHE_DELETE_SUCCESS, MATCH_PLAYER_CACHE_DELETE_FAIL],
    cache: (db) => db.delete(key, isArr)
  };
}