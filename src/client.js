/* global __DEVTOOLS__ */
import 'babel/polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import PouchDB from 'pouchdb';
import createHistory from 'history/lib/createBrowserHistory';

import createStore from './redux/create';
import ApiClient from './helpers/ApiClient';
import DbClient from './helpers/DbClient';
import injectTapEventPlugin from 'react-tap-event-plugin';
import FastClick from 'fastclick';
import {Provider} from 'react-redux';

import getRoutes from './routes';
import makeRouteHooksSafe from './helpers/makeRouteHooksSafe';

import {reduxReactRouter, ReduxRouter} from 'redux-router';
import blackTheme from './views/themes/blackTheme';

const ThemeManager = require('material-ui/lib/styles/theme-manager');

// import io from 'socket.io-client';
/*
Needed for onTouchTap
Can go away when react 1.0 release
Check this repo:
https://github.com/zilverline/react-tap-event-plugin
*/
injectTapEventPlugin();

const client = new ApiClient();
const db = new DbClient();

const dest = document.getElementById('content');
const store = createStore(reduxReactRouter, makeRouteHooksSafe(getRoutes), createHistory, client, db, window.__data);


function startApp() {
  const component = (
    <ReduxRouter routes={getRoutes(store)} />
  );

  const MuiProvider = Provider;
  MuiProvider.prototype.getChildContext = function getChildContext() {
    return { store: store, muiTheme: this.props.muiTheme };
  };

  const extraChildContextTypes = { muiTheme: React.PropTypes.object };
  Object.assign(MuiProvider.childContextTypes, extraChildContextTypes);

  ReactDOM.render(
    <MuiProvider store={store} muiTheme={ThemeManager.getMuiTheme(blackTheme)} key="provider">
      {component}
    </MuiProvider>,
    dest
  );

  if (process.env.NODE_ENV !== 'production') {
    window.React = React; // enable debugger
    window.PouchDB = PouchDB;
    const reactRoot = window.document.getElementById('content');

    if (!reactRoot || !reactRoot.firstChild || !reactRoot.firstChild.attributes || !reactRoot.firstChild.attributes['data-react-checksum']) {
      console.error('Server-side React render was discarded. Make sure that your initial render does not contain any client-side code.');
    }
  }

  if (__DEVTOOLS__) {
    const DevTools = require('./containers/DevTools/DevTools');
    ReactDOM.render(
      <MuiProvider store={store} muiTheme={ThemeManager.getMuiTheme(blackTheme)} key="provider">
        <div>
          {component}
          <DevTools />
        </div>
      </MuiProvider>,
      dest
    );
  }
}

const app = {
  // Application Constructor
  initialize: function() {
    console.log('initialize');
    this.bindEvents();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function() {
    const url = document.URL;
    const isSmart = (url.indexOf("http://") === -1 && url.indexOf("https://") === -1);
    if( isSmart ){
      console.log('isdeviceready?')
      document.addEventListener('deviceready', this.onDeviceReady, false);
      FastClick.attach(document.body, {});
    }
    else{
      startApp();
    }
    // document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicitly call 'app.receivedEvent(...);'
  onDeviceReady: function() {
    console.log('onDeviceReady');
    app.receivedEvent('deviceready');
    startApp();
  },
  // Update DOM on a Received Event
  receivedEvent: function(id) {
    const parentElement = document.getElementById(id);
    console.log('Received Event: ' + id);
  }
};

app.initialize();
