let Colors = require('material-ui/lib/styles/colors');
let ColorManipulator = require('material-ui/lib/utils/color-manipulator');
let Spacing = require('material-ui/lib/styles/spacing');

module.exports = {
  spacing: Spacing,
  fontFamily: 'Roboto, sans-serif',
  palette: {
    primary1Color: '#3F51B5',
    primary2Color: '#303F9F',
    primary3Color: '#C5CAE9',
    accent1Color: '#607D8B',
    accent2Color: '#222326',
    accent3Color: Colors.pinkA100,
    textColor: '#000000',
    alternateTextColor: Colors.white,
    secondaryTextColor: '#FAFAFA',
    canvasColor: '#FAFAFA',
    borderColor: Colors.grey300,
    disabledColor: ColorManipulator.fade(Colors.darkBlack, 0.3),
  },
};
// import Colors from 'material-ui/lib/styles/colors';
// import Spacing from 'material-ui/lib/styles/spacing';
// import {Styles} from 'material-ui';
// import ColorManipulator from 'material-ui/lib/utils/color-manipulator';

// /**
//  *  Light Theme is the default theme used in material-ui. It is guaranteed to
//  *  have all theme variables needed for every component. Variables not defined
//  *  in a custom theme will default to these values.
//  */

// module.exports = {
//   spacing: Spacing,
//   fontFamily: 'Roboto, sans-serif',
//   palette: {
//     primary1Color: '#3F51B5',
//     primary2Color: '#303F9F',
//     primary3Color: '#C5CAE9',
//     accent1Color: '#607D8B',
//     accent2Color: '#222326',
//     accent3Color: Colors.pinkA100,
//     textColor: '#000000',
//     secondaryTextColor: '#FAFAFA',
//     canvasColor: '#FAFAFA',
//     borderColor: Colors.grey300,
//     disabledColor: ColorManipulator.fade(Colors.darkBlack, 0.3),
//   },
// };

// // module.exports = {
// //   spacing: Spacing,
// //   contentFontFamily: 'Roboto, sans-serif',
// //   getPalette() {
// //     console.log('----------------->');
// //     return {
// //       primary1Color: '#3F51B5',
// //       primary2Color: '#303F9F',
// //       primary3Color: '#C5CAE9',
// //       accent1Color: '#607D8B',
// //       accent2Color: '#222326',
// //       accent3Color: Colors.pinkA100,
// //       textColor: '#000000',
// //       secondaryTextColor: '#FAFAFA',
// //       canvasColor: '#FAFAFA',
// //       borderColor: Colors.grey300,
// //       disabledColor: ColorManipulator.fade(Colors.darkBlack, 0.3),
// //     };
// //   },
// //   getComponentThemes(palette, spacing) {
// //     console.log('----------------->');
// //     let cardColor = Colors.grey800;
// //     spacing = spacing || Spacing;
// //     return {
// //       appBar: {
// //         color: palette.primary1Color,
// //         textColor: palette.secondaryTextColor,
// //         height: 64
// //       },
// //       checkbox: {
// //         boxColor: palette.textColor,
// //         checkedColor: palette.primary1Color,
// //         requiredColor: palette.primary1Color,
// //         disabledColor: palette.disabledColor,
// //         labelColor: palette.textColor,
// //         labelDisabledColor: palette.disabledColor,
// //       },
// //       floatingActionButton: {
// //         disabledColor: ColorManipulator.fade(palette.textColor, 0.12),
// //       },
// //       leftNav: {
// //         color: palette.canvasColor,
// //       },
// //       listDivider: {
// //         color: 'black',
// //       },
// //       menu: {
// //          backgroundColor: Colors.white,
// //          containerBackgroundColor: Colors.white,
// //        },
// //        menuItem: {
// //          dataHeight: 32,
// //          height: 48,
// //          hoverColor: 'rgba(0, 0, 0, .035)',
// //          padding: spacing.desktopGutter,
// //          selectedTextColor: palette.accent1Color,
// //        },
// //        menuSubheader: {
// //          padding: spacing.desktopGutter,
// //          borderColor: palette.borderColor,
// //          textColor: palette.primary1Color,
// //        },
// //       paper: {
// //         backgroundColor: palette.canvasColor,
// //         color: palette.textColor,
// //       },
// //       raisedButton: {
// //         color: Colors.grey500,
// //       },
// //       toggle: {
// //         thumbOnColor: Colors.cyan200,
// //         thumbOffColor: Colors.grey400,
// //         thumbDisabledColor: Colors.grey800,
// //         thumbRequiredColor: Colors.cyan200,
// //         trackOnColor: ColorManipulator.fade(Colors.cyan200, 0.5),
// //         trackOffColor: 'rgba(255, 255, 255, 0.3)',
// //         trackDisabledColor: 'rgba(255, 255, 255, 0.1)',
// //       },
// //       slider: {
// //         trackColor: Colors.minBlack,
// //         handleColorZero: cardColor,
// //         handleFillColor: cardColor,
// //         selectionColor: Colors.cyan200,
// //       },
// //     };
// //   }
// // };