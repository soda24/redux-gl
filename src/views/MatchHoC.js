import React, {Component, PropTypes, Children} from 'react';
import {Link} from 'react-router';
import {bindActionCreators} from 'redux';
import DocumentMeta from 'react-document-meta';
import {getToken} from '../redux/modules/auth';
import {isOnline} from '../redux/modules/system';
import {connect} from 'react-redux';
import * as matchActions from '../actions/matchActions';
import {loadRemote as loadMatches, loadCache, storeCache} from '../actions/matchActions';

import {expired, isStateReady} from '../utils';

import {
  getMatchEvents,
} from '../redux/modules/events';

import {
  getById as getMatchById,
} from '../redux/modules/matches';


import {
  loadRemote as loadRemoteEvents,
  loadCache as loadCachedEvents,
  storeCache as storeCacheEvents,
  deleteCache as deleteCacheEvents,
} from '../actions/eventActions';

import {
  loadRemote as loadRemoteMatchPlayers,
  loadCache as loadCachedMatchPlayers,
  storeCache as storeCacheMatchPlayers,
  deleteCache as deleteCacheMatchPlayers,
} from '../actions/matchPlayerActions';

import {
  Err,
  Expired,
  Loading,
  NoInformationError,
  OnlineNeeded,
  RemoteError,
  Waiting,
} from '../components/Messages';


class MatchHoC extends Component {
  constructor(props) {
    super(props);
    this.renderLogic = this.renderLogic.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  static propTypes = {
    match: PropTypes.object,
    online: PropTypes.bool.isRequired,
    matchPlayersStatus: PropTypes.string,
    eventStatus: PropTypes.string,
  }

  static fetchData(getState, dispatch, location, params) {
    const state = getState();
    const matchPlayersStatus = isStateReady(state.matchPlayers);
    const eventStatus = isStateReady(state.events);
    const isLoading = matchPlayersStatus == 'loading' || eventStatus == 'loading';
    const isReady = matchPlayersStatus == 'loaded' && eventStatus == 'loaded';
    const isExpired = matchPlayersStatus == 'expired' || eventStatus == 'expired';
    if (isReady) {
      console.log('matchHoC.notFetch');
      return;
    }
    const matchId = parseInt(params.matchId);
    dispatch(loadCachedEvents(`match/${matchId}/events`)).then((action) => {
      // if (!action.result.data || !action.result.data.length || eventStatus.isExpired) {
      //   setTimeout(() => {
      //     const online = isOnline(getState());
      //     const token = getToken(getState());
      //     if (online && token && isStateReady(getState().events) === 'expired') {
      //       //Traer eventos
      //       dispatch(loadRemoteEvents(getToken(getState()), matchId)).then((payload) => {
      //         dispatch(storeCacheEvents(`match/${matchId}/events`, payload.result)).then((a) => {
      //           setTimeout(() => {
      //             dispatch(loadCachedEvents(`match/${matchId}/events`)).catch((err) => {console.error(err)});
      //           }, 400);
      //         }).catch(function(err){console.error(err)})
      //       });
      //     }
      //   }, 400);
      // }
    }).catch(function(err){console.error(err)})

    dispatch(loadCachedMatchPlayers(`match/${matchId}/players`)).then((action) => {
      if (!action.result.data || !action.result.data.length || matchPlayersStatus.isExpired) {
        setTimeout(() => {
          const online = isOnline(getState());
          const token = getToken(getState());
          if (online && token && (isStateReady(getState().matchPlayers) === 'expired' || !action.result.data || !action.result.data.length)) {
            //Traer eventos
            dispatch(loadRemoteMatchPlayers(getToken(getState()), matchId)).then((payload) => {
              dispatch(storeCacheMatchPlayers(`match/${matchId}/players`, payload.result)).then((a) => {
                setTimeout(() => {
                  dispatch(loadCachedMatchPlayers(`match/${matchId}/players`)).catch((err) => {console.error(err)})
                }, 400);
              }).catch(function(err){console.error(err)})
            });
          }
        }, 400);
      }
    }).catch(function(err){console.error(err)})
  }

  shouldComponentUpdate(nextProps, nextState) {
    // console.log(nextProps);
    // console.log('TournamentsHoC - isLoading:', nextProps.isLoading);
    // console.log('TournamentsHoC - isReady:', nextProps.isReady);
    // console.log('TournamentsHoC - isExpired:', nextProps.isExpired);
    const props = this.props;
    // console.log(nextProps.events.length, this.props.events.length);
    if (nextProps.location.pathname !== this.props.location.pathname) {
      console.log('TournamentsHoC.componentWillUpdate.1 - ', true);
      return true;
    }
    if (nextProps.isReady && this.props.isReady) {
      console.log(nextProps.events.length, this.props.events.length);
      if (nextProps.events.length !== this.props.events.length) {
        console.log('TournamentsHoC.componentWillUpdate.2 - ', true);
        return true;
      }
      console.log('TournamentsHoC.componentWillUpdate.2 - ', false);
      return false;
    }
    if (nextProps.isReady !== props.isReady) {
      console.log('TournamentsHoC.componentWillUpdate.3 - ', true);
      return true;
    }
    if (nextProps.isExpired !== props.isExpired) {
      console.log('TournamentsHoC.componentWillUpdate.4 - ', true);
      return true;
    }
    if (nextProps.isLoading !== props.isLoading) {
      console.log('TournamentsHoC.componentWillUpdate.5 - ', true);
      return true;
    }
    console.log('TournamentsHoC.componentWillUpdate.6 - ', false);
    return false;
  }

  async handleUpdate() {
    const {
      matchPlayersStatus,
      eventStatus,
      online,
      dispatch,
      token
    } = this.props;
    const {matchId, tournamentId} = this.props.params;
    // if (eventStatus === 'expired') {
    //   await dispatch(deleteCacheEvents(`match/${matchId}/events`, true));
    // }
    if (matchPlayersStatus === 'expired') {
      await dispatch(deleteCacheMatchPlayers(`match/${matchId}/players`, true));
    }
    if (online && token) {
      //Traer eventos
      // dispatch(loadRemoteEvents(token, matchId)).then((payload) => {
      //   dispatch(storeCacheEvents(`match/${matchId}/events`, payload.result)).then((a) => {
      //     setTimeout(() => {
      //       dispatch(loadCachedEvents(`match/${matchId}/events`)).catch((err) => {console.error(err)});
      //     }, 400);
      //   }).catch(function(err){console.error(err)})
      // });
      //Traer eventos
      dispatch(loadRemoteMatchPlayers(token, matchId)).then((payload) => {
        dispatch(storeCacheMatchPlayers(`match/${matchId}/players`, payload.result)).then((a) => {
          setTimeout(() => {
            dispatch(loadCachedMatchPlayers(`match/${matchId}/players`)).catch((err) => {console.error(err)})
          }, 400);
        }).catch(function(err){console.error(err)})
      });
    }
    console.log('Aca llamar a las accions que actualizan');
  }

  renderLogic() {
    const {online, data, children, isReady, isLoading, isExpired} = this.props;
    const couldNotExists = true;
    // if (this.isInformationLoaded({...this.props.eventStatus, data, online})) {
    // console.log('isLoading, isExpired, isReady', isLoading, isExpired, isReady);
    if (isLoading) return <Loading message="Loading information" />;
    if (isExpired) return <Expired onClick={this.handleUpdate} />;
    if (isReady) {
        return Children.map(children, function (child) {
          return React.cloneElement(child, {
            parentLoading: isLoading,
            parentReady: isReady,
          })
        }, this)
    }
    else {
      return <NoData message="La información está desactualizada" />;
    }
  }

  render() {
    console.log('MatchHoc.render');
    return <span>{this.renderLogic()}</span>
  }
}

function mergeProps(stateProps, dispatchProps, parentProps) {
  const {matchId, tournamentId} = parentProps.params;
  const match = getMatchById(stateProps, matchId);
  const matchPlayersStatus = isStateReady(stateProps.matchPlayers);
  const eventStatus = isStateReady(stateProps.events);
  console.log('isLoading, isExpired, isReady',
    matchPlayersStatus == 'loading' || eventStatus == 'loading',
    matchPlayersStatus == 'expired' || eventStatus == 'expired',
    matchPlayersStatus == 'loaded' && eventStatus == 'loaded',
    stateProps.matchPlayers.cacheExpirationDate,
    stateProps.events.cacheExpirationDate
  );
  // console.log('MatchHoC - matchPlayersStatus:', matchPlayersStatus, 'eventStatus:', eventStatus);
  // console.log('dispatchProps', dispatchProps);
  return Object.assign({}, parentProps, {
    token: stateProps.auth.token,
    online: stateProps.system.online,
    events: match && getMatchEvents(stateProps, matchId),
    matchPlayersStatus,
    eventStatus,
    ...dispatchProps,
    isLoading: matchPlayersStatus == 'loading' || eventStatus == 'loading',
    isReady: matchPlayersStatus == 'loaded' && eventStatus == 'loaded',
    isExpired: matchPlayersStatus == 'expired', //|| eventStatus == 'expired', //Ya no necesitamos eventStatus expirado porque no vamos a levantar esa info del servidor ni a palos
    // todos: stateProps.todos[parentProps.userId],
  });
}


function mapStateToProps(state) {
  return {
    ...state
  };
}

function finalMapDispatchToProps(dispatch) {
  return {dispatch: dispatch};
}

export default connect(
    mapStateToProps, (a) => { return finalMapDispatchToProps(a); }, mergeProps
)(MatchHoC)
