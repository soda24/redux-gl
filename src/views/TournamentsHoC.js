import React, {Component, PropTypes, Children} from 'react';
import cloneWithProps from 'react-addons-clone-with-props';
import {Link} from 'react-router';
import {bindActionCreators} from 'redux';
import DocumentMeta from 'react-document-meta';
import {connect} from 'react-redux';
import {expired, isStateReady} from '../utils';
import {getToken} from '../redux/modules/auth';
import {isOnline} from '../redux/modules/system';

import * as tournamentActions from '../actions/tournamentActions';
import * as matchActions from '../actions/matchActions';

import {loadRemote as loadTournaments, loadCache, storeCache} from '../actions/tournamentActions';
import {loadRemote as loadMatches, loadCache as loadMatchCache, storeCache as storeMatchCache} from '../actions/matchActions';

import {isLoaded, isRemoteLoaded, isRemoteNeeded, getAll} from '../redux/modules/tournaments';
import {isRemoteLoaded as isRemoteMatchLoaded, isRemoteNeeded as isRemoteMatchNeeded} from '../redux/modules/matches';


import {
  Err,
  Expired,
  Loading,
  NoInformationError,
  AuthNeeded,
  OnlineNeeded,
  RemoteError,
  Waiting,
} from '../components/Messages';


class TournamentsHoC extends Component {
  constructor(props, context) {
    super(props, context);
    this.getRemoteTournaments = this.getRemoteTournaments.bind(this);
  }

  static contextType = {
    store: PropTypes.object,
  }

  static propTypes = {
    online: PropTypes.bool.isRequired,
    data: PropTypes.array,
    error: PropTypes.string,
    matchStatus: PropTypes.string,
    tournamentStatus: PropTypes.string,
  }

  shouldComponentUpdate(nextProps, nextState) {
    // console.log(nextProps);
    // console.log('TournamentsHoC - isLoading:', nextProps.isLoading);
    // console.log('TournamentsHoC - isReady:', nextProps.isReady);
    // console.log('TournamentsHoC - isExpired:', nextProps.isExpired);
    const props = this.props;
    if (nextProps.location.pathname !== this.props.location.pathname) {
      // console.log('TournamentsHoC.componentWillUpdate.1 - ', true);
      return true;
    }
    if (nextProps.isReady && this.props.isReady) {
      // console.log('TournamentsHoC.componentWillUpdate.2 - ', false);
      return false;
    }
    if (nextProps.isReady !== props.isReady) {
      // console.log('TournamentsHoC.componentWillUpdate.3 - ', true);
      return true;
    }
    if (nextProps.isExpired !== props.isExpired) {
      // console.log('TournamentsHoC.componentWillUpdate.4 - ', true);
      return true;
    }
    if (nextProps.isLoading !== props.isLoading) {
      // console.log('TournamentsHoC.componentWillUpdate.5 - ', true);
      return true;
    }
    // console.log('TournamentsHoC.componentWillUpdate.6 - ', false);
    return false;
  }

  getRemoteTournaments() {
    const {dispatch, token} = this.props;
    if (token) {
      dispatch(tournamentActions.loadRemote(token)).then(function(payload) {
        dispatch(tournamentActions.storeCache('tournaments', payload.result)).then(function(a){
          setTimeout(() => {
            dispatch(tournamentActions.loadCache('tournaments'));
          }, 400);
        })
      }).catch((err) => {console.error('dispatch', err);})

      dispatch(matchActions.loadRemote(token)).then(function(payload) {
        dispatch(matchActions.storeCache('matches', payload.result)).then(function(a){
          setTimeout(() => {
            dispatch(matchActions.loadCache('matches'));
          }, 400);
        })
      }).catch((err) => {console.error('dispatch', err);})
    }
  }

  componentWillMount() {
    console.log(this.state, this.props);
    // const {dispatch, online, token, store} = this.props;
    // // For HoC if remote fetching is needed, now we assume that cache returns the information we want
    // dispatch(tournamentActions.loadCache('tournaments')).catch(function(err){
    //   if (!action.result.data || !action.result.data.length) {
    //     setTimeout(function(){
    //       if (online && token) {
    //         //Traer eventos
    //         dispatch(tournamentActions.loadTournaments(token)).then(function(payload) {
    //           dispatch(tournamentActions.storeCache('tournaments', payload.result)).then(function(a){
    //             dispatch(tournamentActions.loadCache('tournaments'));
    //           })
    //         }).catch(function(err){console.error('dispatch', err);})
    //       }
    //     }, 400);
    //   }
    // }).catch(function(err){console.error(err)});


    // //Load Matches
    // dispatch(matchActions.loadCache('matches')).catch(function(err){
    //   if (!action.result.data || !action.result.data.length) {
    //     setTimeout(function(){
    //       if (online && token) {
    //         //Traer Matches
    //         dispatch(matchActions.loadMatches(token)).then(function(payload) {
    //           dispatch(matchActions.storeCache('matches', payload.result)).then(function(a){
    //             dispatch(matchActions.loadCache('matches'));
    //           });
    //         }).catch(function(err){console.error('dispatch', err);});
    //       }
    //     }, 400);
    //   }
    // }).catch(function(err){console.error(err)});
  }

  static fetchData(getState, dispatch) {
    const state = getState();
    const matchStatus = isStateReady(state.matches);
    const tournamentStatus = isStateReady(state.tournaments);

    const isLoading = matchStatus === 'loading' || tournamentStatus === 'loading';
    const isReady = matchStatus === 'loaded' && tournamentStatus === 'loaded';
    const isExpired = matchStatus === 'expired' || tournamentStatus === 'expired';

    // For HoC if remote fetching is needed, now we assume that cache returns the information we want
    dispatch(loadCache('tournaments')).then(function(action){
      if (!action.result.data || !action.result.data.length || isExpired) {
        setTimeout(() => {
          const online = isOnline(getState());
          const token = getToken(getState());
          if (online && token && isStateReady(getState().tournaments) === 'expired') {
            //Traer eventos
            dispatch(loadTournaments(token)).then(function(payload) {
              dispatch(storeCache('tournaments', payload.result)).then(function(a){
                setTimeout(() => {
                  dispatch(loadCache('tournaments'));
                }, 400);
              })
            }).catch(function(err){console.error('dispatch', err);})
          }
        }, 400);
      }
    }).catch(function(err){console.error(err)});


    //Load Matches
    dispatch(loadMatchCache('matches')).then(function(action){
      if (!action.result.data || !action.result.data.length || isExpired) {
        setTimeout(() => {
          const online = isOnline(getState());
          const token = getToken(getState());
          if (online && token && isStateReady(getState().matches) === 'expired') {
            //Traer Matches
            dispatch(loadMatches(token)).then(function(payload) {
              dispatch(storeMatchCache('matches', payload.result)).then(function(a){
                setTimeout(() => {
                  dispatch(loadMatchCache('matches'));
                }, 400);
              });
            }).catch((err) => {console.error('dispatch', err);});
          }
        }, 400);
      }
    }).catch((err) => {console.error(err)});

  }

  renderLogic2() {
    const {history, isReady, isLoading, isExpired, online, tournaments, matches, children, token} = this.props;
    const couldNotExists = true;
    if (isLoading) {
      return <Loading message="Loading information" />;
    }
    if (isExpired) {
      return <Expired onClick={this.getRemoteTournaments} />;
    }
    if (isReady) {
      if (tournaments && _.keys(tournaments).length && matches && _.keys(matches).length) {      
        return Children.map(children, function (child) {
          return cloneWithProps(child, {
            parentLoading: isLoading,
            parentReady: isReady,
          });
        }, this);
      } else {
        if (token) {
          return <NoInformationError message="La información está desactualizada" onClick={this.getRemoteTournaments} />;
        } else {
          return <AuthNeeded onClick={() => {history.pushState(null, '/login');}} />;
        }
      }
    }
  }

  render() {
    return <span>{this.renderLogic2()}</span>
  }

}

  

function mergeProps(stateProps, dispatchProps, parentProps) {
  const matchStatus = isStateReady(stateProps.matches);
  const tournamentStatus = isStateReady(stateProps.tournaments);
  return Object.assign({}, parentProps, {
    token: stateProps.auth.token,
    online: stateProps.system.online,
    tournaments: stateProps.tournaments.data,
    matches: stateProps.matches.data,
    matchStatus,
    tournamentStatus,
    isLoading: matchStatus == 'loading' || tournamentStatus == 'loading',
    isReady: matchStatus == 'loaded' && tournamentStatus == 'loaded',
    isExpired: matchStatus == 'expired' || tournamentStatus == 'expired',
    ...dispatchProps,
    // todos: stateProps.todos[parentProps.userId],
  });
}

function finalMapDispatchToProps(dispatch) {
  return {dispatch: dispatch};
}

function mapStateToProps(state) {
  return {
    ...state
  };
}

export default connect(
    mapStateToProps, (a) => { return finalMapDispatchToProps(a); }, mergeProps
)(TournamentsHoC)
