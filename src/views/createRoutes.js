import React from 'react';
import {Route} from 'react-router';
import App from 'views/App';
// import Widgets from 'views/Widgets';
import Tournaments from 'views/Tournaments';
import TournamentsHoC from 'views/TournamentsHoC';
import MatchHoC from 'views/MatchHoC';
import Matches from 'views/Matches';
import Match from 'views/Match';
import Titulars from 'views/MatchTitulars/Titulars';
import EditTitulars from 'views/MatchTitulars/EditTitulars';
import EditMatchEvents from 'views/MatchTitulars/EditMatchEvents';
import Login from 'views/Login';
import NotFound from 'views/NotFound';

export default function(store) {
  return (
    <Route component={App} >
      <Route component={TournamentsHoC} >
        <Route path="tournaments" component={Tournaments} />
        <Route path="tournaments/:tournamentId/matches" component={Matches} />
        <Route component={MatchHoC} >
          <Route path="tournaments/:tournamentId/matches/:matchId" component={Match} />
          <Route path="tournaments/:tournamentId/matches/:matchId/titulars" component={Titulars} />
          <Route path="tournaments/:tournamentId/matches/:matchId/titulars/:local" component={EditTitulars} />
          <Route path="tournaments/:tournamentId/matches/:matchId/events" component={EditMatchEvents} />
        </Route>
      </Route>
      <Route path="/login" component={Login} />
      <Route path="*" component={NotFound} />
    </Route>
  );
}
