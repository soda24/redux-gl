import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import DocumentMeta from 'react-document-meta';
import _ from 'lodash';
import moment from 'moment';

import config from '../config';

import {getToken, isAboutToLogIn} from '../redux/modules/auth';
import {connect} from 'react-redux';
import * as matchActions from '../actions/matchActions';
import * as eventActions from '../actions/eventActions';

import {FetchingFromServer, ConnectionError} from '../components/Messages';

import {loadRemote as loadMatches, loadCache, storeCache} from '../actions/matchActions';

import {
  isLoaded as areEventLoaded,
  isRemoteLoaded as areRemoteEventLoaded,
  isRemoteNeeded as isEventRemoteNeeded,
  getById as getMatchById,
} from '../redux/modules/matches';

import {
  getByType as getEventsByType,
} from '../redux/modules/events';

import {
  getById as getTournamentById,
} from '../redux/modules/tournaments';

const IconMenu = require('material-ui/lib/menus/icon-menu');
const MenuItem = require('material-ui/lib/menus/menu-item');

function expired(datestring) {
  return moment(datestring, 'YYYYMMDDhhmmdd') < moment()
}


import Colors from 'material-ui/lib/styles/colors'

import {
    Avatar,
    CardHeader,
    Card,
    CardMedia,
    CardTitle,
    CardActions,
    CardText,
    FlatButton,
    FloatingActionButton,
    FontIcon,
    List,
    ListItem,
    ListDivider,
    Menu,
    Paper,
    RaisedButton,

    } from 'material-ui';


class Match extends Component {
  constructor(props) {
    super(props)
    this.isTitularsComplete = this.isTitularsComplete.bind(this);
    this.isInformationReliable = this.isInformationReliable.bind(this);
    this.drawHomeTitularsItem = this.drawHomeTitularsItem.bind(this);
    this.drawAwayTitularsItem = this.drawAwayTitularsItem.bind(this);
  }

  static propTypes = {
    match: PropTypes.object,
    error: PropTypes.string,
    loading: PropTypes.bool,
    load: PropTypes.func.isRequired,
    homeTitulars: PropTypes.array,
    awayTitulars: PropTypes.array,
    tournament: PropTypes.object,
    router: PropTypes.func
  }

  static fetchData(store, props, context) {

  }

  isInformationReliable() {
    const {match, homeTitulars, awayTitulars, tournament} = this.props;
    if (!match || !tournament) return true;
    if (!match.expiresAt || !tournament.expiresAt) return true;
    if (expired(match.expiresAt) || expired(tournament.expiresAt)) return true;
  }

  isTitularsComplete() {
    const {match, homeTitulars, awayTitulars, tournament, error, loading, load} = this.props;
    if (!match || tournament) return false;
  }

  renderLoading() {
      return (
          <Loading />
      );
  }

  renderFetching() {
      return (
          <FetchingFromServer />
      );
  }

  renderConnectionError() {
      return (
          <ConnectionError />
      );
  }

  renderOnlineNoData() {
      var self = this;
      const {history} = this.props;
      var btn = <RaisedButton label='VOLVER' onClick={() => {history.pushState(null, 'matches')}} />;
      return (
          
          <NoData backButton={btn} />
      );
  }

  drawHomeTitularsItem() {
    const {match, tournament, homeTitulars, history} = this.props;
    const {tournamentId, matchId} = this.props.params;
    let rightIcon = <FontIcon style={{width: -1}} className="fa fa-cog" onTouchTap={this.props.action} />;
    let st = {};
    if (homeTitulars.length != tournament.max_players_in_field) {
      // st = {backgroundColor: 'rgba(0, 0, 0, 0.54)', color: 'rgba(255, 255, 255, 0.87)'};
    }
    return (
      <span>
        <ListItem
          leftAvatar={<Avatar style={st} src={config.contentUrl+'/'+match.home_team.logo_path} />}
          rightIcon={rightIcon}
          style={{textAlign: 'center', ...st}}
          primaryText={`TITULARES de ${match.home_team.name} ${homeTitulars.length}/${tournament.max_players_in_field} Cargados`}
          onTouchTap={() => {history.pushState(null, `/tournaments/${tournamentId}/matches/${matchId}/titulars/home`)}}
        >
        </ListItem>
        <ListDivider />
      </span>
    );
  }

  drawAwayTitularsItem() {
    const {match, tournament, awayTitulars, history} = this.props;
    const {tournamentId, matchId} = this.props.params;
    let rightIcon = <FontIcon style={{width: -1}} className="fa fa-cog" onTouchTap={this.props.action} />;
    let st = {};
    if (awayTitulars.length != tournament.max_players_in_field) {
      // st = {backgroundColor: 'rgba(0, 0, 0, 0.54)', color: 'rgba(255, 255, 255, 0.87)'};
    }
    return (
      <span>
        <ListItem
          leftAvatar={<Avatar style={st} src={config.contentUrl+'/'+match.away_team.logo_path} />}
          rightIcon={rightIcon}
          style={{textAlign: 'center', ...st}}
          primaryText={`TITULARES de ${match.away_team.name} ${awayTitulars.length}/${tournament.max_players_in_field} Cargados`}
          onTouchTap={() => {history.pushState(null, `/tournaments/${tournamentId}/matches/${matchId}/titulars/away`)}}
        >
        </ListItem>
        <ListDivider />
      </span>
    );
  }

  renderOfflineNoData() {

  }

  renderApp() {
      const match = this.props.match||{};
      const styles = this.getStyles();
      const {history} = this.props;
      const {tournamentId, matchId} = this.props.params;
      var match_status = 'Desconocido';
      if (match.status == 'p') {
          match_status = 'No comenzado';
      }
      else if (match.status == 'i') {
          match_status = 'En juego';
      }
      else if (match.status == 'o') {
          match_status = 'Finalizado';
      }
      else if (match.status == 's') {
          match_status = 'Suspendido';
      }
      else if (match.status == 'r') {
          match_status = 'Reprogramado';
      }
      let goToButton = (
        <span />
      );
      if (this.isTitularsComplete()) {
          goToButton = (
            <ListItem
                style={{textAlign: 'center'}}
                primaryText={"CARGAR EVENTOS"}
                onTouchTap={(e)=>{e.preventDefault(); history.pushState(null, `/tournaments/${tournamentId}/matches/${matchId}/events`)}}
                >
            </ListItem>
          );
      }
      const synchbtn = (
        <ListItem
            style={{textAlign: 'center', backgroundColor: 'rgba(0, 0, 0, 0.54)', color: 'rgba(255, 255, 255, 0.87)'}}
            primaryText={<span>Última sincronización: {match.events_synchronized_at && moment(match.events_synchronized_at).calendar() || 'Todavía no se ha sincronizado con el servidor...'}</span>}
            rightAvatar={<FloatingActionButton iconClassName="fa fa-refresh" mini={true} onTouchTap={this.syncronize}/>}
            onTouchTap={this.synchronize}
            >
        </ListItem>
        );
      return (
          <Card>
              <CardHeader
               title={match.name}
               avatar={config.contentUrl+'/grass1.png'}/>
              <CardMedia
                overlay={
                  <CardTitle
                    title={match.name}
                  />
                }
              >
                <img src={config.contentUrl+'/grass1.png'}/>
              </CardMedia>
              <CardTitle 
                title={moment(match.match_date).calendar()+' - '+match_status}
              />
              <List>
               <ListDivider />
                <ListItem
                    style={{textAlign: 'center', backgroundColor: 'rgba(0, 0, 0, 0.54)', color: 'rgba(255, 255, 255, 0.87)'}}
                    primaryText={<span>El partido {match.name} a desarrollarse el día {moment(match.match_date).calendar()} en Barker Club, Solomé 213, Banfield, Provincia de Buenos Aires, Argentina.</span>}
                    >
                </ListItem>
                {synchbtn}
                <ListDivider />
                <ListItem
                    style={{textAlign: 'center', backgroundColor: 'rgba(0, 0, 0, 0.54)', color: 'rgba(255, 255, 255, 0.87)'}}
                    primaryText={'Titulares'}
                    >
                </ListItem>
                <ListDivider />
                { this.drawHomeTitularsItem() }
                { this.drawAwayTitularsItem() }
                <ListDivider />
                {goToButton}
                <ListDivider />
                <ListItem
                    style={{textAlign: 'center'}}
                    primaryText={"CERRAR PARTIDO"}
                    onTouchTap={(e)=>{e.preventDefault(); this.closeMatch}}
                    >
                </ListItem>
                <ListDivider />
              </List>
          </Card>
      );
  }

  whatToRender() {
      const tournament = this.props;
      return 'app';
  }

  syncronize() {
    var self = this;
    this.props.flux.getActions('events').syncronize(this.props.match.match_id, this.props.events).then(function(){
      self.props.flux.getActions('events').loadRemoteOverride({match_id: self.props.match.match_id});
    });
  }

  isTitularsComplete() {
      const {match, tournament, homeTitulars, awayTitulars} = this.props;
      if (!match) {
          return false;
      }
      if (homeTitulars.length > tournament.max_players_in_field || homeTitulars.length < tournament.min_players_in_field) {
          return false;
      }
      if (awayTitulars.length > tournament.max_players_in_field || awayTitulars.length < tournament.min_players_in_field) {
          return false;
      }
      return true;
  }

  getStyles() {
      var darkWhite = Colors.darkWhite;
      return {
          footer: {
              backgroundColor: Colors.grey900,
              textAlign: 'center'
          },
          buttons: {
            textAlign: 'center',
            marginBottom: '12px'
          },
          a: {
              color: darkWhite
          },
          p: {
              margin: '0 auto',
              padding: '0',
              color: Colors.lightWhite,
              maxWidth: '335px'
          },
          iconButton: {
              color: darkWhite
          },
          main: {
              margin: 30,
              padding: 10,
              textAlign: 'center',
              fontSize: 20
          }
      };
  }

  render() {
      let whatToRender = this.whatToRender();
      if (whatToRender == 'app') {
          return this.renderApp();
      }

  }
}

function mergeProps(stateProps, dispatchProps, parentProps) {
  let {matchId, tournamentId} = parentProps.params;
  let match = getMatchById(stateProps, matchId);
  // console.log(getEventsByType(stateProps, matchId, 1, match.home_team_id));
  return Object.assign({}, parentProps, {
    match: getMatchById(stateProps, matchId),
    homeTitulars: match && getEventsByType(stateProps, matchId, 1, match.home_team_id),
    awayTitulars: match && getEventsByType(stateProps, matchId, 1, match.away_team_id),
    tournament: getTournamentById(stateProps, tournamentId)
    // ...stateProps,
    // todos: stateProps.todos[parentProps.userId],
  });
}

function mapStateToProps(state) {
  return {
    ...state,
  };
}

export default connect(
    mapStateToProps, matchActions, mergeProps
)(Match);
