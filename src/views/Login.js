import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import DocumentMeta from 'react-document-meta';
import {isLoaded as isAuthLoaded} from '../redux/modules/auth';
import * as authActions from '../actions/authActions';
import {load as loadAuth} from '../actions/authActions';

import FullWidthSection from '../components/FullWidthSection'

import {
    TextField,
    RaisedButton,
  } from 'material-ui';

const style = require('./Login.scss');

@connect(
  state => ({user: state.auth.user}),
  dispatch => bindActionCreators(authActions, dispatch)
)
export default class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  static propTypes = {
    user: PropTypes.object,
    login: PropTypes.func,
    logout: PropTypes.func,
    storeAccount: PropTypes.func,
  }

  static fetchData(store) {
    // if (!isAuthLoaded(store.getState())) {
    //   try {
    //     let a = store.dispatch(loadAuth())
    //     if (a.then) {
    //       a.then().catch(function(err){console.error(err)})
    //     }
    //   }
    //   catch(error) {
    //     console.error(error)
    //   }
    // }
    // return true
  }

  componentWillMount() {
    let {login, storeAccount, storeMe, history, loadAccount} = this.props;
    loadAccount().then((resp) => {
      const {username, password, storeAccount} = resp.result;
      if (username && password && storeAccount) {
        login(username, password).then((action) => {
          storeMe(action.result);
          history.goBack();
          // history.pushState(null, '/tournaments');
        }).catch((err) => {
          console.error(err);
        });
      }
    }).catch((err) => {console.error(err);});

  }

  handleSubmit(event) {
    event.preventDefault();
    let {login, storeAccount, storeMe, history, location} = this.props;
    const username = this.refs.username.getValue();  // need for getDOMNode() call going away in React 0.14
    const password = this.refs.password.getValue();  // need for getDOMNode() call going away in React 0.14
    // const storeAcc = this.refs.storeAccount.getValue();  // need for getDOMNode() call going away in React 0.14
    const storeAcc = {checked: true};
    login(username, password).then((action) => {
      if (storeAcc.checked) {
        storeAccount({username: username, password: password, storeAccount: true});
      }
      storeMe(action.result);
      history.goBack();
      // history.pushState(null, '/tournaments');
    }).catch((err) => {
      console.error(err);
    });
    // input.value = '';
  }

  render() {
    const {user, logout} = this.props;
    let containerStyle = {
      textAlign: 'center',
      paddingTop: '200px'
    };

    let standardActions = [
      { text: 'Okay' }
    ];

    return (
      <FullWidthSection style={containerStyle}>
        <form className={style.logo} onSubmit={::this.handleSubmit}>
            <h1>Por favor ingresa tu usuario y contraseña</h1>
            <TextField ref="username" placeholder="email" defaultValue="nicolas.reynoso@gmail.com"/><br/>
            <TextField ref="password" type="password" placeholder="password" defaultValue="lasagna"/><br/>
            <RaisedButton type="submit" label="INICIO" secondary={true} onClick={::this.handleSubmit} />
        </form>
      </FullWidthSection>
    );
  }
}