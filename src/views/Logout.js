import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import DocumentMeta from 'react-document-meta';
import {isLoaded as isAuthLoaded} from '../redux/modules/auth';
import * as authActions from '../actions/authActions';
import {load as loadAuth} from '../actions/authActions';

import FullWidthSection from '../components/FullWidthSection'

import {
    TextField,
    RaisedButton,
  } from 'material-ui';

const style = require('./Login.scss');

@connect(
  state => ({user: state.auth.user}),
  dispatch => bindActionCreators(authActions, dispatch)
)
export default class Logout extends Component {
  constructor(props, context) {
    super(props, context);
  }
  static propTypes = {
    user: PropTypes.object,
    login: PropTypes.func,
    logout: PropTypes.func,
    storeAccount: PropTypes.func,
  }

  static fetchData(store) {
    // if (!isAuthLoaded(store.getState())) {
    //   try {
    //     let a = store.dispatch(loadAuth())
    //     if (a.then) {
    //       a.then().catch(function(err){console.error(err)})
    //     }
    //   }
    //   catch(error) {
    //     console.error(error)
    //   }
    // }
    // return true
  }

  componentWillMount() {
    const {logout, storeAccount, storeMe, history, deleteCache} = this.props;
      deleteCache('me').then(() => {
        history.pushState(null, '/tournaments');
      }).catch((err) => {console.error(err);});
  }

  render() {
    const {user, logout} = this.props;
    const containerStyle = {
      textAlign: 'center',
      paddingTop: '200px'
    };

    const standardActions = [
      { text: 'Okay' }
    ];
    return (
      <FullWidthSection style={containerStyle}>
        <div className={style.logo}>
          <h1>Hasta luego!</h1>
        </div>
      </FullWidthSection>
    );
  }
}