import React, {Component, PropTypes} from 'react';

import _ from 'lodash'
import {connect} from 'react-redux';
import * as matchActions from '../../actions/matchActions';
import * as eventActions from '../../actions/eventActions';

import FullWidthSection from '../../components/FullWidthSection'

import config from '../../config';

import moment from 'moment';

import { 
  Avatar,
  Checkbox,
  ClearFix,
  Dialog,
  FloatingActionButton,
  FontIcon,
  IconButton,
  Paper,
  Snackbar,
  Styles,
  TextField,
  Toolbar,
  ToolbarGroup,
} from 'material-ui';

const List = require('material-ui/lib/lists/list');
const ListDivider = require('material-ui/lib/lists/list-divider');
const ListItem = require('material-ui/lib/lists/list-item');
const IconMenu = require('material-ui/lib/menus/icon-menu');
const MenuItem = require('material-ui/lib/menus/menu-item');

const Colors = Styles.Colors;
const Typography = Styles.Typography;

import {guid} from '../../utils';

import {
  isLoaded as areEventLoaded,
  isRemoteLoaded as areRemoteEventLoaded,
  isRemoteNeeded as isEventRemoteNeeded,
  getById as getMatchById,
} from '../../redux/modules/matches';

import {
  getByType as getEventsByType,
  add as addEvent,
  storeCache as storeCacheEvents,
} from '../../redux/modules/events';

import {
  getById as getTournamentById,
} from '../../redux/modules/tournaments';

import {
  filterByLocal as filterMatchPlayersByLocal,
} from '../../redux/modules/matchPlayers';

import PlayerListBox from './PlayerListBox';
import BackBox from './BackBox';


class EditMatchTitulars extends React.Component {

  static propTypes = {
    match: PropTypes.object,
    teamId: PropTypes.int,
    titulars: PropTypes.array,
    players: PropTypes.array,
    tournament: PropTypes.object,
    history: PropTypes.object,
  }

  state = {
    message: ''
  }

  constructor(props, context) {
    super(props, context);
    this.setTitular = this.setTitular.bind(this);
    this.removeEvent = this.removeEvent.bind(this);
    this.backAction = this.backAction.bind(this);
    this.confirmTitulars = this.confirmTitulars.bind(this);
    this.onItemTouchTap = this.onItemTouchTap.bind(this);
    this.state.message = '';
  }

  shouldComponentUpdate(nextProps, nextState) {
    const {match, teamId, titulars, players, tournament} = nextProps
    if (match && tournament && players && players.length && tournament.id) {
      return true
    }
    return false
  }

  setTitular(titular) {
    const {match, titulars, teamId, storeCache, add} = this.props;
    const {matchId} = this.props.params
    let exist = _.find(titulars, function(e){
      parseInt(titular.profile_id) == e.profile_id && e.event_type == 1
    })
    if (exist) {
      console.error("El jugador ya esta como titular")
      return
    }
    let profileId = titular.profile_id
    let evtId = `${matchId}|${profileId}`
    let evt = {
      id: evtId,
      event_id: evtId,
      external_event_id: evtId,
      team_id: parseInt(teamId),
      profile_id: parseInt(titular.profile_id),
      position: parseInt(titular.position),
      is_initial_event: true,
      half_time: 0,
      time: 0,
      type: 'titular',
      event_type: 1,
      status: 'i',
      match_date: match.match_date,
      match_id: parseInt(matchId),
    }
    const key = `match/${matchId}/events/${evtId}`;
    storeCache(key, evt).then(function(payload){
      add(evt)
    }).catch(function(e){
      console.error(e);
    });
  };

  removeEvent(eventId) {
    const {match, titulars, teamId, deleteCache, retrieveCache, remove} = this.props;
    const {matchId} = this.props.params;
    const key = `match/${matchId}/events/${eventId}`;
    return deleteCache(key).then(function(e){
      retrieveCache(key).then((e) => { 
        // console.log(e);
      }).catch((err) => {console.error(err);}) //Si no hacia el retriveCache de alguna manera no lo esta borrando no se porque
      remove(eventId);
    }).catch(function(e){
      console.error(e);
    });
  }

  backAction() {
    const {history} = this.props;
    const {matchId, tournamentId} = this.props.params;
    const url = `/tournaments/${tournamentId}/matches/${matchId}`;
    history.pushState(null, url);
  }

  hasEventOtherThanTitulars(key) {
    const {retrieveCache} = this.props;
    return retrieveCache(key).then((resp) => {
      const events = resp.result.data;
      const eventOtherThanTitulars = _.filter(events, (e) => {
        // console.log(e.event_type, e.is_initial_event, e);
        return !e.is_initial_event && e.status != 'd';
      });
      // console.log('!!eventOtherThanTitulars.length', eventOtherThanTitulars.length, !!eventOtherThanTitulars.length);
      return !!eventOtherThanTitulars.length;
    }).catch((err) => { console.error(err); });
  }
  
  async onItemTouchTap(event, item) {
    // _.each(getEventsByType(stateProps, matchId, 1, match[`${local}_team_id`]), (e));
    const {retrieveCache, loadCache, history} = this.props;1
    const {matchId, tournamentId, local} = this.props.params;
    const {teamId} = this.props;
    if (item.key === 'clear_titulars') {
      const key = `match/${matchId}/events`;
      // console.log('await this.hasEventOtherThanTitulars(key)', await this.hasEventOtherThanTitulars(key));
      if (await this.hasEventOtherThanTitulars(key)) {
        this.setState({...this.state, message: "Debe limpiar los eventos del partido primero"});
        this.refs.errorSnack.show();
        return;
      }
      else {
        retrieveCache(key).then((resp) => {
          const events = resp.result.data;
          const teamTitularEvents = _.filter(events, (e) => {
            // console.log(e);
            // console.log(e.is_initial_event, e.status, e.team_id, teamId);
            // console.log(e.is_initial_event, e.status != 'd', e.team_id == teamId);
            if (e.is_initial_event && e.status != 'd' && e.team_id == teamId) {
              return e;
            }
          });
          const promises = [];
          _.each(teamTitularEvents, (e) => {
            promises.push(this.removeEvent(e.id));
          })
          Promise.all(promises).then((la) => {
            // console.log(la);
          }).catch((err) => {
            console.error(err);
          });
          // console.log('!!eventOtherThanTitulars.length', eventOtherThanTitulars.length, !!eventOtherThanTitulars.length);
        }).then().catch((err) => {
          console.error(err);
        }).then(() => {
          
          loadCache(key).then(() => {
            history.pushState(null, `/tournaments/${tournamentId}/matches/${matchId}`);
          })
        });
      }
    }
    if (item.key === 'confirm_titulars') {
      const key = `match/${matchId}/teams/${local}/confirm_titulars`;
      this.confirmTitulars();
    }
  }

  confirmTitulars(key) {
    console.log(key);
  }

  render() {
    const self = this;
    let disabled = true;
    const {match, players, titulars, tournament} = this.props;
    if (tournament && titulars.length < tournament.max_players_in_field ) {
      disabled = false;
    }
    return (
      <FullWidthSection>
        {((el) => {
          if (players.length) {
            return (
              <List>
                <BackBox
                  ref="back_box"
                  action={this.backAction}
                  rightIcon={
                    <IconMenu
                      openDirection={'bottom-left'}
                      iconButtonElement={
                        <FontIcon style={{width: -1, left: '-15px'}} className="fa fa-cog" />
                      }
                      onItemTouchTap={this.onItemTouchTap}
                    >
                      <MenuItem key={'confirm'} primaryText="Confirmar" />
                      <MenuItem key={'clear_titulars'} primaryText="Limpiar Todo" />
                    </IconMenu>
                  }
                  />
                <PlayerListBox
                  disabled={disabled}
                  titulars={titulars}
                  players={players}
                  match={match}
                  tournament={tournament}
                  setTitular={this.setTitular}
                  removeTitular={this.removeEvent}
                />
              </List>
            );
          }
          else {return <span />}
        })()}
      <Snackbar
      ref="errorSnack"
      action="Cerrar"
      message={this.state.message}
      autoHideDuration={7000}
      onActionTouchTap={() => {this.refs.errorSnack.dismiss();}}
      />

      </FullWidthSection>
    );
  }
}

function mergeProps(stateProps, dispatchProps, parentProps) {
  let {matchId, tournamentId, local} = parentProps.params;
  let match = getMatchById(stateProps, matchId);
  return Object.assign({}, parentProps, {
    token: stateProps.auth.token,
    online: stateProps.system.online,
    match: getMatchById(stateProps, matchId),
    teamId: match && match[`${local}_team_id`],
    titulars: match && getEventsByType(stateProps, matchId, 1, match[`${local}_team_id`]),
    players: match && filterMatchPlayersByLocal(stateProps, matchId, local),
    tournament: getTournamentById(stateProps, tournamentId),
    eventStatus: {
      cacheLoading: stateProps.events.cacheLoading,
      cacheLoaded: stateProps.events.cacheLoaded,
      cacheError: stateProps.events.cacheError,
      remoteLoading: stateProps.events.remoteLoading,
      remoteLoaded: stateProps.events.remoteLoaded,
      remoteError: stateProps.events.remoteError,
    },
    ...dispatchProps
    // ...stateProps,
  // todos: stateProps.todos[parentProps.userId],
  });
}

function mapStateToProps(state) {
  return {
  ...state,
  };
}

export default connect(
  mapStateToProps, eventActions, mergeProps
)(EditMatchTitulars);