import React, {Component, PropTypes} from 'react';

import _ from 'lodash'
import {connect} from 'react-redux';
import * as matchActions from '../../actions/matchActions';
import * as eventActions from '../../actions/eventActions';

import FullWidthSection from '../../components/FullWidthSection'

import config from '../../config';

import BackBox from './BackBox';

import moment from 'moment';

import { 
  Avatar,
  Checkbox,
  ClearFix,
  Dialog,
  FloatingActionButton,
  FontIcon,
  IconButton,
  Paper,
  Styles,
  TextField,
  Toolbar,
  ToolbarGroup,
} from 'material-ui';

const List = require('material-ui/lib/lists/list');
const ListDivider = require('material-ui/lib/lists/list-divider');
const ListItem = require('material-ui/lib/lists/list-item');

const IconMenu = require('material-ui/lib/menus/icon-menu');
const MenuItem = require('material-ui/lib/menus/menu-item');

const Colors = Styles.Colors;
const Typography = Styles.Typography;


import {
  isLoaded as areEventLoaded,
  isRemoteLoaded as areRemoteEventLoaded,
  isRemoteNeeded as isEventRemoteNeeded,
  getById as getMatchById,
} from '../../redux/modules/matches';

import {
  getByType as getEventsByType,
} from '../../redux/modules/events';

import {
  getById as getTournamentById,
} from '../../redux/modules/tournaments';


let event_types = {
  1:  'titular',
  2:  'captain',
  3:  'goalkeeper',
  4:  'player_in',
  5:  'player_out',
  6:  'goal',
  7:  'goal_against',
  8:  'yellow_card',
  9:  'green_card',
  10: 'blue_card',
  11: 'red_card'
}

class MatchTitulars extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      zDepth: 0,
      team_id: null,
    }
    this.backAction = this.backAction.bind(this);
  }


  backAction() {
    const {history} = this.props;
    const {tournamentId, matchId} = this.props.params;
    history.pushState(null, `/tournaments/${tournamentId}/matches/${matchId}`);
  }

  
  render() {
    const {history, match, tournament} = this.props;
    const {tournamentId, matchId} = this.props.params;
    return (
      <FullWidthSection>
        <List>
          <BackBox
            ref="back_box"
            action={this.backAction}
            />
          <TeamBox match={match} goTo={(local)=>{history.pushState(null, `/tournaments/${tournamentId}/matches/${matchId}/titulars/${local}`)}} />
        </List>
        
      </FullWidthSection>
    );
  }
}






class TeamBox extends React.Component {
  constructor(props, context) {
    super(props, context);
  }


  render() {
    var self = this;
    const {match, goTo} = this.props;
    return (
      <span>
        {((a) => {
          if (match.home_team && match.away_team) {
            return (
              <span>
              <ListItem
                onTouchTap={(e)=>{e.preventDefault(); goTo('home')}}
                leftAvatar={<Avatar src={config.contentUrl+'/'+match.home_team.logo_path} />}
                primaryText={match.home_team.name}
                secondaryTextLines={2}>
              </ListItem>
              <ListDivider />
              <ListItem
                onTouchTap={(e)=>{e.preventDefault(); goTo('away')}}
                leftAvatar={<Avatar src={config.contentUrl+'/'+match.away_team.logo_path} />}
                primaryText={match.away_team.name}
                secondaryTextLines={2}>
              </ListItem>
              <ListDivider />
              </span>
            );
          }
          else {
            return <span />
          }
        })()}
      </span>
    );
  }
};


function mergeProps(stateProps, dispatchProps, parentProps) {
  let {matchId, tournamentId} = parentProps.params
  let match = getMatchById(stateProps, matchId)
  return Object.assign({}, parentProps, {
  match: getMatchById(stateProps, matchId),
  homeTitulars: match && getEventsByType(stateProps, matchId, 1, match.home_team_id),
  awayTitulars: match && getEventsByType(stateProps, matchId, 1, match.away_team_id),
  tournament: getTournamentById(stateProps, tournamentId)
  // ...stateProps,
  // todos: stateProps.todos[parentProps.userId],
  });
}

function mapStateToProps(state) {
  return {
  ...state,
  };
}

export default connect(
  mapStateToProps, matchActions, mergeProps
)(MatchTitulars)