const React = require('react');
import { 
  FontIcon,
} from 'material-ui';

const ListDivider = require('material-ui/lib/lists/list-divider');
const ListItem = require('material-ui/lib/lists/list-item');


export default class BackBox extends React.Component {

  static propTypes = {
    action: React.PropTypes.func,
    rightIcon: React.PropTypes.object,
  }

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const {rightIcon} = this.props || (<span/>);
    return (
      <span>
        <ListItem
          leftIcon={<FontIcon className="fa fa-chevron-left" onTouchTap={this.props.action}/>}
          rightIcon={rightIcon}
          primaryText={"ATRAS"}
          secondaryTextLines={2}>
        </ListItem>
        <ListDivider />
      </span>
    );
  }
};