import React, {Component, PropTypes} from 'react';

import _ from 'lodash'
import {connect} from 'react-redux';
import * as matchActions from '../../actions/matchActions';
import * as eventActions from '../../actions/eventActions';

import FullWidthSection from '../../components/FullWidthSection'

import config from '../../config';

import moment from 'moment';

import {guid} from '../../utils';

import { 
  Avatar,
  Checkbox,
  ClearFix,
  Dialog,
  FloatingActionButton,
  FontIcon,
  IconButton,
  List,
  ListItem,
  ListDivider,
  Paper,
  Snackbar,
  Styles,
  TextField,
  Toolbar,
  ToolbarGroup,
} from 'material-ui';

const Colors = Styles.Colors;
const Typography = Styles.Typography;

import {
  EventButtonBox,
  HalfTimeBox,
  TimeBox,
  TeamBox,
  EndBox,
  BackBox,
  PlayerBox,
  EventList,
  PlayerSubstituteBox,
} from './EditMatchEventsComponents'

import {
  getById as getMatchById,
} from '../../redux/modules/matches';

import {
  getByType as getEventsByType,
  getMatchEvents,
} from '../../redux/modules/events';

import {
  getById as getTournamentById,
} from '../../redux/modules/tournaments';

import {
  filterByLocal as filterMatchPlayersByLocal,
} from '../../redux/modules/matchPlayers';

import {
  loadRemote as loadRemoteEvents,
  loadCache as loadCachedEvents,
  storeCache as storeCacheEvent,
  deleteCache as deleteCacheEvent,
  clear as clearEvents,
  saveRemote,
  add as addEvent,
  remove as removeEvent,
  retrieveCache as listEventCache,
} from '../../actions/eventActions';

import * as temporalEventActions from '../../actions/temporalEventActions';

const event_types = {
  1:  'titular',
  2:  'captain',
  3:  'goalkeeper',
  4:  'player_in',
  5:  'player_out',
  6:  'goal',
  7:  'goal_against',
  8:  'yellow_card',
  9:  'green_card',
  10: 'blue_card',
  11: 'red_card',
}

const event_types_trans = {
  'titular': 'titular',
  'captain': 'capitán',
  'goalkeeper': 'arquero',
  'player_in': 'entra',
  'player_out': 'sale',
  'goal': 'gol',
  'goal_against': 'gol en contra',
  'yellow_card': 'tarjeta amarilla',
  'green_card': 'tarjeta verde',
  'blue_card': 'tarjeta azul',
  'red_card': 'tarjeta roja',
}


class DebugMatchEvents extends React.Component {

  state = {
    eventListIsShown: false,
    remoteError: [],
  }

  static contextTypes = {
    store: PropTypes.object.isRequired,
  }

  static propTypes = {
    match: PropTypes.object,
    teamId: PropTypes.number,
    titulars: PropTypes.array,
    players: PropTypes.array,
    tournament: PropTypes.object,
    history: PropTypes.object,
  }

  constructor(props, context) {
    super(props, context);
    this.backAction = this.backAction.bind(this);
    this.renderBox = this.renderBox.bind(this);
    this.boxToBeShown = this.boxToBeShown.bind(this);
    this.getPlayers = this.getPlayers.bind(this);
    this.setPlayer = this.setPlayer.bind(this);
    this.setPlayer2 = this.setPlayer2.bind(this);
    this.deleteEvent = this.deleteEvent.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  isEventRemote(evtId) {
    return false
  }

  onSave() {
    const self = this;
    const {token, listEventCache, saveRemote, loadRemoteEvents, storeCacheEvent, loadCachedEvents, deleteCacheEvent, clearEvents} = this.props;
    const {matchId, tournamentId} = this.props.params;
    const {store} = this.context;
    const key = `match/${matchId}/events`;

    listEventCache(key).then(function(res){
      const evts = res.result.data;
      _.each(evts, function(e){
      console.log(e.event_type, e.type, e.external_event_id);
      });
      saveRemote(token, matchId, evts).then(function(a){
      deleteCacheEvent(key, true).catch(function(err){console.error(err)});
      //Traigo la nueva información desde el server.
      loadRemoteEvents(token, matchId).then(function(payload){
        storeCacheEvent(`match/${matchId}/events`, payload.result).then(function(a){
        clearEvents();
        loadCachedEvents(`match/${matchId}/events`).catch(function(err){console.error(err)});
        }).catch(function(err){console.error(err)});
      });
      }).catch(function(err){
      console.error(err);
      if (err.body) {
        self.setState({remoteError: err.body.data});
      }
      })
    })
  }

  deleteEvent(evt) {
    const {tevent, setPlayer, match, deleteCacheEvent, storeCacheEvent, removeEvent} = this.props;
    const {matchId, tournamentId} = this.props.params;
    const {store} = this.context;
    const {snackbar} = this.refs;
    const evtId = evt.event_id;
    const key = `match/${matchId}/events/${evtId}`;

    //TODO: En realidad habría que actualizar el status a 'd' si es remoto
    //Y solo borrar si no es remote
    if (isNaN(evt.event_id)) {
      deleteCacheEvent(key).then(function(la){
      removeEvent(key);
      snackbar.show();
      })
    }
    else {
      evt.status = 'd'
      storeCacheEvent(key, evt).then(function(la){
      removeEvent(key);
      snackbar.show();
      })
    }

  }

  setPlayer(profileId, position) {
    const {tevent, setPlayer, resetTime, match} = this.props;
    const {matchId, tournamentId} = this.props.params;
    const {store} = this.context;
    console.log('setPlayer', profileId, position);
    const self = this;
    setPlayer(profileId, position);
    if (tevent.type != 'change') {
      let tmp = {...tevent};
      tmp.profile_id = parseInt(profileId);
      tmp.position = parseInt(position);
      tmp.match_id = parseInt(matchId);
      tmp.tournament_id = parseInt(tournamentId);
      tmp.status = 'i';
      tmp.match_date = match.match_date;
      tmp.is_initial_event = false;
      tmp.event_type = parseInt(_.invert(event_types)[tmp.type]);
      let evtId = guid();
      let key = `match/${matchId}/events/${evtId}`;
      tmp.id = evtId;
      tmp.event_id = evtId;
      tmp.external_event_id = evtId;
      store.dispatch(storeCacheEvent(key, tmp)).then(function(la){
      tmp._id = key;
      store.dispatch(addEvent(tmp));
      if (tevent.type == 'red_card') {
        tmp = {...tmp}
        tmp.ref_external_event_id = tmp.id
        tmp.event_type = 5
        tmp.type = 'player_out'
        tmp.ref_event_id = tmp.id
        evtId = guid()
        key = `match/${matchId}/events/${evtId}`
        tmp.id = evtId
        tmp.event_id = evtId
        tmp.external_event_id = evtId
        tmp._id = key
        store.dispatch(storeCacheEvent(key, tmp)).then(function(la){
        store.dispatch(addEvent(tmp));
        console.log('storeCacheEvent2', key, tmp);
        resetTime();
        self.setState({eventListIsShown: !this.state.eventListIsShown});
        })
        .catch(function(err){
        console.error('fallo agregando player out por tajeta roja. Habria que borrar la tarjeta roja?')
        })
      }
      resetTime();
      self.setState({eventListIsShown: !this.state.eventListIsShown});
      })
    }
  }

  setPlayer2(profileId, position) {
    console.log('setPlayer2(profileId, position)');
    const {tevent, setPlayer, setPlayer2, resetTime, match} = this.props;
    const {matchId, tournamentId} = this.props.params;
    const {store} = this.context;
    setPlayer2(profileId, position);
    console.log('ACA CONSTRUIR LOS DOS EVENTOS el de PLAYER_IN y el de PLAYER_OUT y poner los ids de referencia');

    //PLAYER1
    let tmp = {...tevent};
    tmp.match_id = parseInt(matchId);
    tmp.tournament_id = parseInt(tournamentId);
    tmp.status = 'i';
    tmp.match_date = match.match_date;
    tmp.is_initial_event = false;
    tmp.event_type = 5;
    tmp.type = 'player_out';
    let evtId = guid();
    let key = `match/${matchId}/events/${evtId}`;
    tmp.id = evtId;
    tmp.external_event_id = evtId;
    tmp.event_id = evtId;
    store.dispatch(storeCacheEvent(key, tmp)).then(function(la){
      tmp._id = key;
      store.dispatch(addEvent(tmp));
      // console.log('storeCacheEvent', key, tmp);
      tmp = {...tmp};
      tmp.ref_external_event_id = tmp.id;
      tmp.profile_id = parseInt(profileId);
      tmp.position = parseInt(position);
      tmp.event_type = 4;
      tmp.type = 'player_in';
      tmp.ref_event_id = tmp.id;
      evtId = guid();
      key = `match/${matchId}/events/${evtId}`;
      tmp.id = evtId;
      tmp.event_id = evtId;
      tmp.external_event_id = evtId;
      tmp._id = key;
      store.dispatch(storeCacheEvent(key, tmp)).then(function(la){
      store.dispatch(addEvent(tmp))
      console.log('storeCacheEvent2', key, tmp);
      resetTime();
      this.setState({eventListIsShown: !this.state.eventListIsShown});
      })
      .catch(function(err){
      console.error('fallo agregando player in. Habria que borrar el player out');
      })
    }).catch(function(err){
      console.error(err);
    });
  }

  getPlayers() {
    const {homePlayers, awayPlayers, match, tournament, tevent} = this.props;
    if (parseInt(tevent.team_id) == match.home_team_id) return homePlayers;
    if (parseInt(tevent.team_id) == match.away_team_id) return awayPlayers;
  }

  backAction() {
    this.setState({eventListIsShown: false});
    const {history, tevent, resetPlayer2, resetPlayer, resetTeam, resetEvent, resetTime, resetHalfTime} = this.props;
    const {tournamentId, matchId} = this.props.params;
    if (tevent.profile2_id) {           
      resetPlayer2();
      return;
    }
    if (tevent.profile_id) {
      resetPlayer();
      return;
    }
    if (tevent.team_id) {
      resetTeam();
      return;
    }
    if (tevent.type) {
      resetEvent();
      return;
    }
    if (tevent.time) {
      resetTime();
      return;
    }
    if (tevent.half_time) {
      resetHalfTime();
      return;
    }
    resetTime();
    let backUrl = `/tournaments/${tournamentId}/matches/${matchId}`
    history.pushState(null, backUrl);
    return;
  }

  boxToBeShown() {
    if (this.state.eventListIsShown) {
      return 'event_list';
    }
    const {tevent} = this.props
    console.log('boxToBeShown()', tevent)
    if (!tevent.half_time) {
      return 'half_time';
    }
    if (!tevent.time) {
      return 'time';
    }
    if (!tevent.type) {
      return 'event';
    }
    if (!tevent.team_id) {
      return 'team';
    }
    if (!tevent.profile_id) {
      return 'titular';
    }
    if (!tevent.profile2_id) {           
      return 'substitute';
    }
  }

  renderBox() {
  const {tevent} = this.props
    if (this.state.eventListIsShown) {
      return 'event_list';
    }
    if (!tevent.half_time) {
      return 'half_time';
    }
    if (!tevent.time) {
      return 'time';
    }
    if (!tevent.type) {
      return 'event';
    }
    if (!tevent.team_id) {
      return 'team';
    }
    if (!tevent.profile_id) {
      return 'titular';
    }
    if (!tevent.profile2_id) {           
      return 'substitute';
    }
  }

  getStyles() {
    var darkWhite = Colors.darkWhite;
    return {
      footer: {
        backgroundColor: Colors.grey900,
        textAlign: 'center'
      },
      FullWidthSection: {
        margin: 0
      },
      buttons: {
      textAlign: 'center',
      marginBottom: '12px'
      },
      a: {
        color: darkWhite
      },
      p: {
        margin: '0 auto',
        padding: '0',
        color: Colors.lightWhite,
        maxWidth: '335px'
      },
      iconButton: {
        color: darkWhite,
        margin: 0,
        width: 24,
        height: 24,
      },
      main: {
        margin: 30,
        padding: 10,
        textAlign: 'center',
        fontSize: 20
      },
      group: {
        margin: 30,
        padding: 10,
        textAlign: 'center',
        width: '33%'
      },
      mainFlex: {
        marginTop: 10,
        marginBottom: 10,
        padding: 10
      },
      blueBox: {
        padding: 1,
        margin: 0,
        // backgroundColor: '#007FFF',
        color: 'white',
        textAlign: 'center',
        verticalAlign: 'middle'
      },
      boxContainer: {

      }

    };
  }

  listPlayersOnField(props) {
    const time = props.tevent.time;
    const halfTime = props.tevent.half_time;
    let titulars = props.matchEvents.filter(function(e){
      return parseInt(e.event_type) == 1;
    }).map(function(e){
      return e.profile_id;
    })
    let changeEvents = props.matchEvents.filter(function(e){
      return _.indexOf([4, 5], parseInt(e.event_type)) >= 0;
    })
    //4: player_in, 5: player_out
    _.each(changeEvents, function(e){
      try {
      let event_half_time = parseInt(e.half_time);
      let event_time = parseInt(e.time);
      if ( (event_half_time < halfTime )|| (event_half_time == halfTime && event_time <= time)) {
        if (e.event_type == 5) {
        titulars = titulars.filter(function(profileId){
          return profileId!=e.profile_id;
        })
        }
        else if (e.event_type == 4) {
        titulars.push(e.profile_id);
        }
      }
      } catch(error) {
      console.error(error);
      }
    })
    return titulars;
  }


  render() {
    console.log('DebugMatchEvents.render');
    let self = this;
    const playersOnField = this.listPlayersOnField(this.props);
    const styles = this.getStyles();
    const boxToBeShown = this.boxToBeShown();;
    const {homePlayers, awayPlayers, match, tournament, tevent, matchEvents, setHalfTime} = this.props;
    const {setTime, setEventType, setTeam, setPlayer, setPlayer2} = this.props;
    const backTitle = 'ATRAS';
    const box = <span />;
    const backBox = (<BackBox ref="back_box" styles={styles} title={backTitle} action={this.backAction} />);
    return (
      <FullWidthSection>
      <List>
        {backBox}
      </List>
      <EventList
        errors={this.state.remoteError}
        homePlayers={homePlayers}
        awayPlayers={awayPlayers}
        data={matchEvents}
        styles={styles}
        match={match}
        action={(evt)=>{self.deleteEvent(evt)}}
        saveAction={this.onSave}
        all={true}
        />
      <Snackbar
        ref="snackbar"
        action="cerrar"
        message="El evento ha sido borrado con éxito"
        autoHideDuration={2200}
        onActionTouchTap={() => {this.refs.snackbar.dismiss()}}/>
      <Snackbar
      ref="remoteError"
      action="revisar"
      message="Hubo errores en la sincronización con el servidor"
      autoHideDuration={2200}
      onActionTouchTap={() => {this.refs.remoteError.dismiss(); self.setState({eventListIsShown: true})}}/>
      </FullWidthSection>
    );
  }
}

function mergeProps(stateProps, dispatchProps, parentProps) {
  console.log('----------->');
  const {matchId, tournamentId, local} = parentProps.params;
  const match = getMatchById(stateProps, matchId);
  return Object.assign({}, parentProps, {
  token: stateProps.auth.token,
  match: getMatchById(stateProps, matchId),
  homePlayers: match && filterMatchPlayersByLocal(stateProps, matchId, 'home'),
  awayPlayers: match && filterMatchPlayersByLocal(stateProps, matchId, 'away'),
  matchEvents: match && getMatchEvents(stateProps, matchId),
  tournament: getTournamentById(stateProps, tournamentId),
  tevent: stateProps.temporalEvent,
  ...dispatchProps
  // ...stateProps,
  // todos: stateProps.todos[parentProps.userId],
  });
}

function mapStateToProps(state) {
  return {
  ...state,
  };
}

export default connect(
  mapStateToProps,
  {
  ...temporalEventActions,
  storeCacheEvent,
  deleteCacheEvent,
  loadRemoteEvents,
  loadCachedEvents,
  clearEvents,
  addEvent,
  removeEvent,
  listEventCache,
  saveRemote,
  },
  mergeProps
)(DebugMatchEvents);