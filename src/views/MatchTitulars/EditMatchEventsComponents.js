import React, {Component, PropTypes} from 'react';
import {
  Avatar,
  FontIcon,
  ListItem,
  ListDivider,
  TextField,
} from 'material-ui';

import config from '../../config';

const eventTypesTrans = {
  'titular': 'titular',
  'captain': 'capitán',
  'goalkeeper': 'arquero',
  'player_in': 'entra',
  'player_out': 'sale',
  'goal': 'gol',
  'goal_against': 'gol en contra',
  'yellow_card': 'tarjeta amarilla',
  'green_card': 'tarjeta verde',
  'blue_card': 'tarjeta azul',
  'red_card': 'tarjeta roja',
}

const yellowCardImg = require('../../www/img/yellow_card.png');
const redCardImg = require('../../www/img/red_card.png');
const goalImg = require('../../www/img/soccerball.png');
const goalAgainstImg = require('../../www/img/red_soccerball.png');
const playerInImg = require('../../www/img/green_arrow.png');
const playerOutImg = require('../../www/img/red_arrow.png');

export class EventButtonBox extends Component {

  static propTypes = {
    action: React.PropTypes.func,
    styles: React.PropTypes.object,
    selected: React.PropTypes.string,
  }

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const {styles, selected} = this.props;
    return (
      <span>
        <ListItem
          rightIcon={<FontIcon style={{right: 24}} className="fa fa-chevron-right" />}
          onTouchTap={this.props.action.bind(true, 'goal')}
          primaryText="GOL"
          >
        </ListItem>
        <ListDivider />
        <ListItem
          rightIcon={<FontIcon style={{right: 24}} className="fa fa-chevron-right" />}
          onTouchTap={this.props.action.bind(true, 'goal_against')}
          primaryText="GOL EN CONTRA"
          >
        </ListItem>
        <ListDivider />
        <ListItem
          rightIcon={<FontIcon style={{right: 24}} className="fa fa-chevron-right" />}
          onTouchTap={this.props.action.bind(true, 'yellow_card')}
          primaryText="TARJETA AMARILLA"
          >
        </ListItem>
        <ListDivider />
        <ListItem
          rightIcon={<FontIcon style={{right: 24}} className="fa fa-chevron-right" />}
          onTouchTap={this.props.action.bind(true, 'red_card')}
          primaryText="TARJETA ROJA"
          >
        </ListItem>
        <ListDivider />
        <ListItem
          rightIcon={<FontIcon style={{right: 24}} className="fa fa-chevron-right" />}
          onTouchTap={this.props.action.bind(true, 'change')}
          primaryText="CAMBIO"
          >
        </ListItem>
        <ListDivider />
        <ListItem
          rightIcon={<FontIcon style={{right: 24}} className="fa fa-chevron-right" />}
          onTouchTap={this.props.action.bind(true, 'other')}
          primaryText="OTROS..."
          >
        </ListItem>
        <ListDivider />
      </span>

    );
  }
};


export class HalfTimeBox extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <span>
        <ListItem
          rightIcon={<FontIcon style={{right: 24}} className="fa fa-chevron-right" />}
          onTouchTap={this.props.action.bind(true, 1)}
          primaryText="PRIMER TIEMPO"
          secondaryTextLines={2}
          >
        </ListItem>
        <ListDivider />
        <ListItem
          rightIcon={<FontIcon style={{right: 24}} className="fa fa-chevron-right" />}
          onTouchTap={this.props.action.bind(true, 2)}
          primaryText="SEGUNDO TIEMPO"
          secondaryTextLines={2}
          >
        </ListItem>
        <ListDivider />
      </span>
    );
  }
};

export class TimeBox extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.action = this.action.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  action(a, b) {
    this.props.action(this.refs.input.getValue());
  }

  handleKeyPress(e) {
    if (e.charCode === 13 && e.target.value) {
        this.action('second', this.refs.input.getValue());
    }
  }

  render() {
    return (
      <span>
        <ListItem
          rightIcon={<FontIcon style={{marginTop: 30, right: 24}} onTouchTap={this.action.bind(true, 'second')} className="fa fa-chevron-right" />}
          primaryText={<TextField floatingLabelText="Minuto" type="number" ref="input" defaultValue={this.props.value} onKeyPress={this.handleKeyPress} />}
          >
        </ListItem>
        <ListDivider />
      </span>
    );
  }
};

export class TeamBox extends React.Component {
  constructor(props, context) {
    super(props, context);
  }


  render() {
    const {match, action} = this.props;
    return (
      <span>
        <ListItem
          leftAvatar={<Avatar style={{backgroundImage: 'url('+config.contentUrl+'/team/no_escudo.png'+')', backgroundSize: 'contain'}} src={match.home_team && config.contentUrl+'/'+match.home_team.logo_path} />}
          rightIcon={<FontIcon style={{right: 24}} className="fa fa-chevron-right" />}
          onTouchTap={action.bind(true, match.home_team_id)}
          primaryText={match.home_team && match.home_team.name}
          >
        </ListItem>
        <ListDivider />
        <ListItem
          leftAvatar={<Avatar style={{backgroundImage: 'url('+config.contentUrl+'/team/no_escudo.png'+')', backgroundSize: 'contain'}} src={match.away_team && config.contentUrl+'/'+match.away_team.logo_path} />}
          rightIcon={<FontIcon style={{right: 24}} className="fa fa-chevron-right" />}
          onTouchTap={action.bind(true, match.away_team_id)}
          primaryText={match.away_team && match.away_team.name}
          >
        </ListItem>
        <ListDivider />
      </span>
    );
  }
};

export class EndBox extends React.Component {
  constructor(props, context) {
    super(props, context);
  }


  render() {
    return (
      <div className="row">
        <div style={styles.boxContainer} className="col-xs-12">
          <div style={styles.blueBox} className="box-row">
            <RaisedButton style={{
            width: '100%'
          }} label='FINALIZAR EDICION'>
            </RaisedButton>
          </div>
        </div>
      </div>
    );
  }
};

export class BackBox extends React.Component {
  constructor(props, context) {
    super(props, context);
  }


  render() {
    const {styles, action, title} = this.props;
    return (
      <span>
        <ListItem
          rightIcon={<FontIcon style={{right: 24}} className="fa fa-chevron-right" />}
          onTouchTap={action}
          primaryText={title||'ATRAS'}
          >
        </ListItem>
        <ListDivider />
      </span>
    );
  }
};

export class PlayerBox extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  static propTypes = {
    players: PropTypes.array
  }

  render() {
    const {action, playersOnField} = this.props;
    let {players} = this.props;
    let data = <span />;
    players = players.filter(function(p){
      return _.indexOf(playersOnField, p.profile_id) >= 0;
    })
    data = players.map(function(player) {
      return (
        <span key={`player-${player.profile_id}`}>
          <ListItem
            leftAvatar={<Avatar style={{backgroundImage: 'url('+config.contentUrl+'/user/no_avatar.png'+')', backgroundSize: 'contain'}} src={config.contentUrl+'/'+player.avatar_path} />}
            rightIcon={<FontIcon style={{right: 24}} className="fa fa-chevron-right" />}
            onTouchTap={(e)=>{e.preventDefault();action(player.profile_id)}}
            primaryText={player.name}
            >
          </ListItem>
          <ListDivider />
        </span>
      );
    }, this)
    return (
      <span>
        {data}
      </span>
    );
  }
};

export class PlayerSubstituteBox extends Component {
  constructor(props, context) {
    super(props, context);
  }

  static propTypes = {
    players: PropTypes.array
  }

  render() {
    const {playersOnField, action} = this.props;
    let {players} = this.props;
    let data = <span />;
    players = players.filter(function(p){
      return _.indexOf(playersOnField, p.profile_id) < 0;
    })
    data = players.map(function(player) {
      return (
        <span key={`player-substitute-${player.profile_id}`}>
          <ListItem
            leftAvatar={<Avatar style={{backgroundImage: 'url('+config.contentUrl+'/user/no_avatar.png'+')', backgroundSize: 'contain'}} src={config.contentUrl+'/'+player.avatar_path} />}
            rightIcon={<FontIcon style={{right: 24}} className="fa fa-chevron-right" />}
            onTouchTap={(e)=>{e.preventDefault();action(player.profile_id)}}
            primaryText={player.name}
            >
          </ListItem>
          <ListDivider />
        </span>
      );
    }, this)
    return (
      <span>
        {data}
      </span>
    );
  }
};

          // style={{
          // height: '48px',
          // textAlign: 'right',
          // marginBottom: 4,
          //     backgroundColor: Colors.limeA400,
          //     fontSize: 'x-large',
          //     height: '100%',
          //     // paddingTop: 15,
          // color:'gray'}}

export class EventList extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const {data, action, homePlayers, awayPlayers, saveAction, match, errors, all} = this.props;
    let evts = data.filter(function(ev) {
      return ev.status != 'd' && (all || _.indexOf([4, 5, 6, 7, 8, 9, 10, 11], ev.event_type) >= 0);
    });
    let players = {};
    _.each(homePlayers.concat(awayPlayers), function(player){
      players[player.profile_id] = player;
    });
    evts = _.sortByAll(evts, ['half_time', 'time']);
    const evs = evts.map((ev, i) => {
      const time = <span>{ev.half_time+'T - '+ev.time+'`'}</span>;
      const player = players[ev.profile_id];
      let team = match.home_team;
      if (ev.team_id === match.away_team_id) {
        team = match.away_team;
      }
      const error = _.find(errors, function(err){ 
        return (err.event_id == ev.event_id || err.event_id == ev.id);
      })
      let icon = '';
      switch (ev.event_type_label || ev.type) {
        case 'yellow_card':
          icon = yellowCardImg;
          break;
        case 'red_card':
          icon = redCardImg;
          break;
        case 'goal':
          icon = goalImg;
          break;
        case 'goal_against':
          icon = goalAgainstImg;
          break;
        case 'player_in':
          icon = playerInImg;
          break;
        case 'player_out':
          icon = playerOutImg;
          break;
        default:
          icon = '';
          break
        break;
      }

      return (
        <span key={`event-${ev.id}-`}>
          <ListItem
            leftAvatar={<Avatar src={icon} />}
            rightIcon={<FontIcon style={{right: 24}} className="fa fa-remove" onTouchTap={(e)=>{e.preventDefault(); action(ev)}}/>}
            primaryText={
              <span>
              <Avatar src={config.contentUrl + '/' + team.logo_path} />
              {time}
              {error && <small style={{color: "red"}}>&nbsp;{error && error.message}</small>}
              </span>}
            secondaryText={player.name}
            >
          </ListItem>
          <ListDivider />
        </span>
      );
    }, this);
    return <span>
      {evs}
      </span>;
  }
}
