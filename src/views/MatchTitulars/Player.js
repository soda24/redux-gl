import React, {Component, PropTypes} from 'react';
import _ from 'lodash'

import { 
    Avatar,
    Checkbox,
    FontIcon,
    IconButton,
    TextField,
} from 'material-ui';
import config from '../../config';

const List = require('material-ui/lib/lists/list');
const ListDivider = require('material-ui/lib/lists/list-divider');
const ListItem = require('material-ui/lib/lists/list-item');


export default class Player extends React.Component {
    static propTypes = {
        setTitular: React.PropTypes.func.isRequired,
        unsetTitular: React.PropTypes.func.isRequired,
        disabled: React.PropTypes.bool,
        pkey: React.PropTypes.string,
        data: React.PropTypes.object,
    }

    constructor() {
        super()
        this.onToggle = this.onToggle.bind(this)
        this.renderInput = this.renderInput.bind(this)
        this.handleChangeNumber = this.handleChangeNumber.bind(this)
        this.handleOnBlur = this.handleOnBlur.bind(this)
        this.state = {
            input: false,
        }
    }

    handleChangeNumber(e) {
        if (e.charCode === 13 && e.target.value) {
            this.props.setTitular(this.props.data.profile_id, e.target.value)
            this.setState({...this.state, input: false})
        }
    }

    handleOnBlur(e) {
        if (e.target.value) {
            this.props.setTitular(this.props.data.profile_id, e.target.value)
        }
        this.setState({...this.state, input: false})
    }

    onToggle() {
        const {setTitular, unsetTitular, isSelected, data, disabled, pkey} = this.props;
        if (isSelected) return unsetTitular(data.profile_id)
        this.setState({...this.state, input: true})
    }

    renderInput() {
        return <ShirtInput placeholder="Camiseta" onKeyPress={this.handleChangeNumber} onBlur={this.handleOnBlur} />
    }

    render() {
        const {setTitular, unsetTitular, isSelected, data, disabled, pkey} = this.props;
        return (
            <span key={pkey}>
                <ListItem
                    onTouchTap={this.onToggle}
                    leftAvatar={<Avatar style={{backgroundImage: 'url('+config.contentUrl+'/user/no_avatar.png'+')', backgroundSize: 'contain'}} src={config.contentUrl+'/'+data.avatar_path} />}
                    rightIcon={
                        <Checkbox
                           style={{right: '20px'}}
                           defaultChecked={isSelected}
                           disabled={disabled}
                           onTouchTap={(e) => {e.preventDefault(); unsetTitular(data.profile_id)}}
                           />
                    }
                    primaryText={
                        <span>
                        <strong>
                        {
                            (() => (data.position && isSelected)?`(${data.position}) `:"")()
                        }
                        </strong>
                        {
                            (
                                (ch) => this.state.input?this.renderInput():`${data.name}`
                            )()
                        }
                        </span>
                    }
                    secondaryTextLines={2}
                    disabled={this.props.disabled}
                    >

                </ListItem>
                <ListDivider />
            </span>
        );
        return <span />
    }
}

class ShirtInput extends React.Component {
    componentDidMount() {
        this.refs.shirt_number.focus()
    }
    render() {
        return <TextField type="number" ref="shirt_number" placeholder="Camiseta" onKeyPress={this.props.onKeyPress} onBlur={this.props.onBlur} />
    }
}
