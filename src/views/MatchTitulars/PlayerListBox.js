import React, {Component, PropTypes} from 'react';
import _ from 'lodash';
// import FullWidthSection from '../../components/FullWidthSection';
import {
  Snackbar,
  Styles,
} from 'material-ui';

var Colors = Styles.Colors;

import Player from './Player'

export default class PlayerListBox extends Component {

  static propTypes = {
    players: PropTypes.array,
    titulars: PropTypes.array,
    disabled: PropTypes.bool,
    action: PropTypes.func,
    tournament: PropTypes.object,
    unaction: PropTypes.func,
    handleSave: PropTypes.func,
    team_id: PropTypes.number
  }

  constructor(props, context) {
    super(props, context);
    this.setTitular = this.setTitular.bind(this);
    this.unsetTitular = this.unsetTitular.bind(this);
    this.handleSave = this.handleSave.bind(this);
    let titulars = props.titulars.map(function(t) {
      return t;
    })
    this.state = {
      titulars: titulars
    }
  }

  handleSave() {
    this.props.handleSave(this.state.titulars);
  }

  unsetTitular(profile_id) {
    const {removeTitular, setTitular, titulars, players, tournament} = this.props;
    const titular = _.find(titulars, function(p) {return p.profile_id==profile_id});
    if (titular) removeTitular(titular.id);
  }

  setTitular(profile_id, number) {
    const {removeTitular, setTitular, titulars, players, tournament} = this.props;
    let numberExist = _.find(titulars, function(p) {return p.position==number})
    let a = this.refs.snackbar;
    // a.setProps({message: "askjdsalkdjaslkdjaskldjas"})
    if (numberExist) {
      this.refs.snackbar.show();
      return;
    }
    let player = _.find(players, function(p) {return p.profile_id==profile_id});
    let titular = _.find(titulars, function(p) {return p.profile_id==profile_id});
    if (!player) {console.error('No exist el profile_id '+profile_id); return}
    if (titular) {
      removeTitular(titular.id);
    }
    else {
      if (titulars.length < this.props.tournament.max_players_in_field) {
        player.position = number;
        setTitular({profile_id: player.profile_id, position: number});
        // state.titulars.push(player);
      }
    }
  }

  componentDidMount() {
    this.setState({...this.state, titulars: this.props.titulars});
  }

  render() {
    const {titulars, match, players, tournament} = this.props;
    var disabled = false;
    var enableSave = false;
    if (titulars.length >= tournament.min_players_in_field && titulars.length <= tournament.max_players_in_field) {
      enableSave = true;
    }
    if (titulars.length >= tournament.max_players_in_field) {
      disabled = true;
    }
    return (
      <span>
        {((jsx) => {
          if (!players || !players.length) {
            return <span />
          }
          else {
            return players.map(function(player) {
              let titular = _.find(titulars, function(t) {return parseInt(t.profile_id)==parseInt(player.profile_id)});
              let isSelected = Boolean(titular);
              return <Player
              key={`player-${player.profile_id.toString()}`}
                isSelected={isSelected} 
                pkey={player.profile_id.toString()+'-'+(isSelected?'1':'2')+'-'+(this.props.disabled?'1':'2')}
                disabled={(titulars.length >= tournament.max_players_in_field) && !isSelected}
                data={(titular && {...player, position: titular.position})||player}
                setTitular={this.setTitular}
                unsetTitular={this.unsetTitular}
                />
            }, this);
          }
        })()}
        <Snackbar
          ref="snackbar"
          action="cerrar"
          message="El número ya está siendo utilizado"
          autoHideDuration={2200}
          onActionTouchTap={() => {this.refs.snackbar.dismiss()}}/>
      </span>
    );
  }
};
