import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {bindActionCreators} from 'redux';
import DocumentMeta from 'react-document-meta';
import {connect} from 'react-redux';

import {getToken} from '../redux/modules/auth';
import * as tournamentActions from '../actions/tournamentActions';

import {isLoaded, isRemoteLoaded, isRemoteNeeded, getAll} from '../redux/modules/tournaments';
import {loadRemote as loadTournaments, loadCache, storeCache} from '../actions/tournamentActions';
import {listPendingByTournament} from '../redux/modules/matches';

import {isRemoteLoaded as isRemoteMatchLoaded, isRemoteNeeded as isRemoteMatchNeeded} from '../redux/modules/matches';
import {loadRemote as loadMatches, loadCache as loadMatchCache, storeCache as storeMatchCache} from '../actions/matchActions';

import AppCanvas from 'material-ui/lib/app-canvas'
import Avatar from 'material-ui/lib/avatar'
import RaisedButton from 'material-ui/lib/raised-button'
import Paper from 'material-ui/lib/paper'
import ListItem from 'material-ui/lib/lists/list-item'
import List from 'material-ui/lib/lists/list'
import ListDivider from 'material-ui/lib/lists/list-divider'
import FontIcon from 'material-ui/lib/font-icon'

import Colors from 'material-ui/lib/styles/colors'

import {FetchingFromServer, ConnectionError} from '../components/Messages';

import FullWidthSection from '../components/FullWidthSection'


class Tournaments extends Component {

  static propTypes = {
  tournaments: PropTypes.array,
  pendingByTournament: PropTypes.array,
  error: PropTypes.string,
  loading: PropTypes.bool
  }


  static fetchData(store, props, context) {

  }

  render() {
    const {history, tournaments, error, loading, load, pendingByTournament} = this.props;
    let refreshClassName = 'fa fa-refresh';
    if (loading) {
      refreshClassName += ' fa-spin';
    }
    const styles = require('./Widgets.scss');
    if (!this.props.matches || !this.props.matches.length) {
      if (this.props.fetch_status == 'fetching') {
        return this.renderFetching();
      }
      else if (this.props.fetch_status == 'error') {
        return this.renderConnectionError();
      }
    }
    let t = _.sortByAll(this.props.tournaments, ['match_date', 'match_id']);
    let self = this
    let tours = (tournaments||[]).map(function(tournament, idx){
      return (
        <span key={`tournament-${tournament.id}`}>
          <ListItem
            style={{textAlign: 'center'}}
            onTouchTap={(e) => {e.preventDefault(); history.pushState(null, `/tournaments/${tournament.id}/matches`)}}
            primaryText={tournament.name}
            secondaryText={
              <span style={{height: 20}}>Partidos pendientes {pendingByTournament[tournament.id] || 0}</span>
            }
            secondaryTextLines={2}>
          </ListItem>
          <ListDivider />
        </span>
        );
    }, this);
    if (!tours) {
      return <span />;
    }
    return (
      <div>
        <FullWidthSection>
          <List>
          {tours}
          </List>
        </FullWidthSection>
      </div>
    )
  }
}


function mergeProps(stateProps, dispatchProps, parentProps) {
  // console.log(stateProps, dispatchProps, parentProps)
  return Object.assign({}, parentProps, {
  ...stateProps,
  // todos: stateProps.todos[parentProps.userId],
  });
}


function mapStateToProps(state) {
  return {
    token: state.auth.token,
    tournaments: getAll(state),
    pendingByTournament: listPendingByTournament(state),
    error: state.tournaments.error,
    loading: state.tournaments.loading,
  };
}

export default connect(
  mapStateToProps, tournamentActions, mergeProps
)(Tournaments)