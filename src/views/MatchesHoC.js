import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {bindActionCreators} from 'redux';
import DocumentMeta from 'react-document-meta';
import {isLoaded, isRemoteLoaded, isRemoteNeeded, getAll} from '../redux/modules/matches';
import {getToken} from '../redux/modules/auth';
import {connect} from 'react-redux';
import * as matchActions from '../actions/matchActions';
import {loadRemote as loadMatches, loadCache, storeCache} from '../actions/matchActions';

class MatchesHoC extends Component {
  constructor(props) {
    super(props)
  }

  static propTypes = {
    matches: PropTypes.array,
    error: PropTypes.string,
    loading: PropTypes.bool,
  }

  static fetchData(store, props, context) {
    // // For HoC if remote fetching is needed, now we assume that cache returns the information we want
    // var a = store.dispatch(loadCache('matches'))
    // if (a.then) {
    //   a.catch(function(err){
    //     console.error(err)
    //   }).then(function(l){
    //     if (isRemoteNeeded(store.getState())) {
    //       console.log('Pendings are expired, remote load needed')
    //       if (!isRemoteLoaded(store.getState())) {
    //         console.log('Remote wasnt loaded so we can retrieve data')
    //         let token = getToken(store.getState())
    //         store.dispatch(loadMatches(token)).then(function(payload) {
    //           store.dispatch(storeCache('matches', payload.result)).then(function(a){
    //             store.dispatch(loadCache('matches'))
    //           })
    //         }).catch(function(err){
    //           console.error('dispatch', err)
    //         })
    //       }
    //     }
    //   })
    // }
  }

  render() {
    console.log('MatchesHoc.render');
    return (this.props.children);
  }
}


function mergeProps(stateProps, dispatchProps, parentProps) {
  // console.log('MatchHoC - matchPlayersStatus:', matchPlayersStatus, 'eventStatus:', eventStatus);
  return Object.assign({}, parentProps, {
    ...stateProps,
    // todos: stateProps.todos[parentProps.userId],
  });
}


function mapStateToProps(state) {
  return {
    token: state.auth.token,
    matches: getAll(state),
    error: state.matches.error,
    loading: state.matches.loading,
  };
}

export default connect(
    mapStateToProps, matchActions, mergeProps
)(MatchesHoC)