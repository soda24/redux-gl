import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {bindActionCreators} from 'redux';
import DocumentMeta from 'react-document-meta';
import {isLoaded, isRemoteLoaded, isRemoteNeeded, getAll} from '../redux/modules/matches';
import {getToken} from '../redux/modules/auth';
import {connect} from 'react-redux';
import * as matchActions from '../actions/matchActions';
import {loadRemote as loadMatches, loadCache, storeCache} from '../actions/matchActions';
import config from '../config';

import FullWidthSection from '../components/FullWidthSection';

import {
    Avatar,
    List,
    ListItem,
    ListDivider,

    } from 'material-ui';


class Matches extends Component {
  constructor(props) {
    super(props)
  }

  static propTypes = {
    matches: PropTypes.array,
    error: PropTypes.string,
    loading: PropTypes.bool,
  }

  static fetchData(store, props, context) {

  }

  render() {
    const {matches, error, loading, load, params} = this.props;
    const {history} = this.props;
    let refreshClassName = 'fa fa-refresh';
    if (loading) {
      refreshClassName += ' fa-spin';
    }
    const styles = require('./Widgets.scss');
    // console.log('---->', matches);
    return (
      <FullWidthSection>
        <List>
         <ListDivider />
         {matches && matches.length &&
            matches.map((match) => 
              <span>
              <ListItem
                  style={{textAlign: 'center',}}
                  leftAvatar={<Avatar src={config.contentUrl+'/'+match.home_team.logo_path} />}
                  rightAvatar={<Avatar src={config.contentUrl+'/'+match.away_team.logo_path} />}
                  onTouchTap={(e)=>{e.preventDefault(history.pushState(null, `/tournaments/${params.tournamentId}/matches/${match.id}`))}}
                  primaryText={match.name}
                  secondaryText={match.match_date}
                  >
              </ListItem>
              <ListDivider />
              </span>
            )
        }
        </List>
      </FullWidthSection>
    );
  }
}


function mergeProps(stateProps, dispatchProps, parentProps) {
  // console.log(stateProps, dispatchProps, parentProps)
  return Object.assign({}, parentProps, {
    ...stateProps,
    // todos: stateProps.todos[parentProps.userId],
  });
}


function mapStateToProps(state) {
  return {
    token: state.auth.token,
    matches: getAll(state),
    error: state.matches.error,
    loading: state.matches.loading,
  };
}

export default connect(
    mapStateToProps, matchActions, mergeProps
)(Matches)

// Another question

// In this example from the documentation, for messages/:id to show:

// ```
//     <Route component={App}>
//       <Route path="about" component={About}/>
//       <Route path="inbox" component={Inbox}>
//         {/* Add the route, nested where we want the UI to nest */}
//         <Route path="messages/:id" component={Message}/>
//       </Route>
//     </Route>
// ```


// Inbox must render {this.props.children}

// Now, is it possible to not render the parent, and only render Message?

// I need all the fetching logic in Inbox but only render Message.

// What do you guys think is the best way to handle this?

// This could be a solution:
// ```
//     <Route component={App}>
//       <Route path="about" component={About}/>
//       <Route component={InboxFetchLogic}>
//         <Route path="inbox" component={Inbox}/>
//         <Route path="messages/:id" component={Message}/>
//       </Route>
//     </Route>
// ```
