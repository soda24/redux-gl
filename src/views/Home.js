import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import DocumentMeta from 'react-document-meta';
import {isLoaded as isAuthLoaded} from '../redux/modules/auth';
import * as authActions from '../actions/authActions';

import FullWidthSection from '../components/FullWidthSection';

import {
    TextField,
    RaisedButton,
  } from 'material-ui';

const logoImage = require('./logos/no_escudo.png');
const style = require('./Home.scss');

@connect(
  state => ({user: state.auth.user, token: state.auth.token, }),
  dispatch => bindActionCreators(authActions, dispatch)
)
export default class Home extends Component {
  constructor(props, context) {
    super(props, context);
  }
  static propTypes = {
    user: PropTypes.object,
    login: PropTypes.func,
    logout: PropTypes.func,
    storeAccount: PropTypes.func,
  }

  componentDidMount() {
    const {online, token, history} = this.props;
    if (!token) {
      setTimeout(() => {
        history.pushState(null, '/login');
      }, 10000)
    }
  }

  render() {
    const {user, logout, history} = this.props;
    let containerStyle = {
      textAlign: 'center',
      paddingTop: '200px'
    };

    let standardActions = [
      { text: 'Okay' }
    ];

    return (
      <FullWidthSection style={containerStyle}>
        <div className={style.logo}>
          <h1>BIENVENIDO A</h1>
          <img src={logoImage} />
          <br />
          <RaisedButton label="IR A PARTIDOS" secondary={true} onClick={() => history.pushState(null, '/tournaments') } />
        </div>
      </FullWidthSection>
    );
  }
}
