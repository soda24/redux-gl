import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import DocumentMeta from 'react-document-meta';
import {isLoaded as isInfoLoaded} from '../redux/modules/info';
import {isLoaded as isAuthLoaded, getToken, isAboutToLogIn} from '../redux/modules/auth';
import {isOffline, isOnline} from '../redux/modules/system';
import {loadMe, loadAccount, logout, login, storeMe} from '../actions/authActions';
import {setOnline} from '../actions/systemActions';

import AppBar from 'material-ui/lib/app-bar';
import AppCanvas from 'material-ui/lib/app-canvas';
import Menu from 'material-ui/lib/menu';
import LinearProgress from 'material-ui/lib/linear-progress';
import Colors from 'material-ui/lib/styles/colors';
import Typography from 'material-ui/lib/styles/typography';

import AppLeftNav from '../components/AppLeftNav';

const title = 'React Redux Example';
const description = 'All the modern best practices in one example.';
const image = 'https://react-redux.herokuapp.com/logo.jpg';

const meta = {
  title,
  description,
  meta: {
    charSet: 'utf-8',
    property: {
      'og:site_name': title,
      'og:image': image,
      'og:locale': 'en_US',
      'og:title': title,
      'og:description': description,
      'twitter:card': 'summary',
      'twitter:site': '@erikras',
      'twitter:creator': '@erikras',
      'twitter:title': title,
      'twitter:description': description,
      'twitter:image': image,
      'twitter:image:width': '200',
      'twitter:image:height': '200'
    }
  }
};

@connect(
    state => ({
      user: state.auth.user,
      token: state.auth.token,
      isAboutToLogIn: state.auth.loggingIn || state.auth.loading,
    }),
    dispatch => bindActionCreators({logout, login, loadAccount, storeMe, setOnline}, dispatch))
export default class App extends Component {
  constructor(props, context) {
    super(props, context)
    this._onLeftIconButtonTouchTap = this._onLeftIconButtonTouchTap.bind(this);
  }

  static propTypes = {
    children: PropTypes.object.isRequired,
    user: PropTypes.object,
    token: PropTypes.string,
    isAboutToLogIn: PropTypes.bool,
    logout: PropTypes.func.isRequired,
    login: PropTypes.func.isRequired,
    history: PropTypes.object,
  }

  static contextTypes = {
    store: PropTypes.object.isRequired,
    muiTheme: PropTypes.object,
  }

  static fetchData(getState, dispatch) {
    const promises = [];
    if (!isInfoLoaded(getState())) {
      // promises.push(store.dispatch(loadInfo()).then());
    }
    if (!isAuthLoaded(getState())) {
      promises.push(dispatch(loadMe()));
    }
    return Promise.all(promises).catch();
  }

  componentWillMount() {
    this.registerSystem();
    // const {history} = this.props;
    // history.pushState(null, );
  }

  componentWillReceiveProps(nextProps) {
    const {storeMe, login, loadAccount} = nextProps;
    if (this.props.token && !nextProps.token) {
      loadAccount().then(
        function(d){
          let {username, password} = d.result;
          if (username && password) {
            login(username, password).then(function(a){
              storeMe(a.result);
            }).catch(function(err){
              console.error(err)
            })
          }
        }
      )
    }
  }

  _onLeftIconButtonTouchTap() {
      this.refs.leftNav.toggle();
  }

  registerSystem() {
    //Hardcoded to be online
    this.props.setOnline(true);
    // console.log(this.props.history);
  }

  handleLogout(event) {
    event.preventDefault();
    this.props.logout().then(() => {
      history.pushState(null, '/login');
    });
  }

  getMenues() {
    const {user, token} = this.props;
    const menuItems = [
      { route: '/tournaments', text: 'Partidos' },
    ];
    if (!!user && token) {
      menuItems.push({ route: '/logout', text: 'Desconectar' });
    }
    else {
      menuItems.push({ route: '/login', text: 'Conectar' });
    }
    return menuItems;
  }

  render() {
    const {user} = this.props;

     return (
         <AppCanvas predefinedLayout={1}>

             <AppBar
                 onLeftIconButtonTouchTap={this._onLeftIconButtonTouchTap}
                 title={'GRAN LIGA'}
                 zDepth={1}
                 />
             <AppLeftNav menuItems={this.getMenues()} router={this.context.router} ref="leftNav" history={this.props.history} />
             {this.props.children}
         </AppCanvas>
     );
  }
}