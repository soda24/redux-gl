module.exports = {
  development: {
    isProduction: false,
    // contentUrl: 'http://localhost:5001/static/media',
    contentUrl: 'http://granliga.com/static/media',
    // apiUrl: 'granliga.com',
    apiUrl: 'http://localhost',
    port: process.env.PORT || 3000,
    apiPort: process.env.APIPORT || 80,
    apiPort2: 10000,
    app: {
      name: 'React Redux Example Development'
    }
  },
  production: {
    isProduction: true,
    contentUrl: 'http://granliga.com/static/media',
    apiUrl: 'http://granliga.com',
    port: process.env.PORT || 3000,
    apiPort: process.env.APIPORT || 80,
    apiPort2: 10000,
    app: {
      name: 'React Redux Example Production'
    }
  }
}[process.env.NODE_ENV || 'development'];
