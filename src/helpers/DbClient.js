import PouchDB from 'pouchdb';
// import config from 'config';

PouchDB.setMaxListeners(20);
/*
 * This silly underscore is here to avoid a mysterious "ReferenceError: ApiClient is not defined" error.
 * See Issue #14. https://github.com/erikras/react-redux-universal-hot-example/issues/14
 *
 * Remove it at your own risk.
 */
 // const db = new PouchDB('observer-events')

class DbClient_ {

  constructor(db) {
    this.db = new PouchDB('observer-events');
    this.put = this.put.bind(this);
    this._put = this._put.bind(this);
    this._putEach = this._putEach.bind(this);
    this._get = this._get.bind(this);
    this.delete = this.delete.bind(this);
  }

  _get(key) {
    const db = this.db;
    return new Promise((resolve, reject) => {
      try {
        db.get(key).then((doc) => {
          resolve(doc);
        }).catch((err) => {
          console.error(err);
          reject(err);
        });
      }
      catch(error){
        console.error(error);
        reject(error);
      }
    });
  }

  _list(key) {
    let db = this.db;
    return new Promise((resolve, reject) => {
      try {
        db.allDocs({
          include_docs: true,
          attachments: true,
          startkey: `${key}/`,
        }).then((arr) => {
          const data = arr.rows.map((row) => {
            return row.doc;
          }).filter((row) => {
            return row._id && row._id.startsWith(key);
          });
          resolve(
            {
              data: data,
            }
          )
        }).catch((err) => {
          console.error(err);
          reject(err);
        });
      }
      catch (error) {
        console.error(error);
        reject(error);
      }
    });
  }

  put(key, data, expires) {
    console.log('NO ACTUALIZAR UN DOCUMENTO SI NO CAMBIO NADA YA QUE SE GUARDA UN HISTORICO');
    // 6. Don't just update docs for the hell of it
    // Every time you modify a document, another revision is added to its revision history – think Git. Except unlike Git, these revisions contain the full document data (not just the diffs), which can take up a lot of space on disk. So if nothing changed in a document, don't bother put()ing it again.
    if (data instanceof Array) {
      return this._putEach(key, data, expires);
    }
    else {
      return this._put(key, data, expires);
    }
  }

  _put(key, data, expires) {
    let db = this.db;
    return new Promise((resolve, reject) => {
      try {
        db.get(key).then((doc) => {
          if (data._rev) delete data._rev;
          doc = {...doc, ...data, expiresAt: expires};
          db.put(doc).then((res) => {
            resolve(doc);
          }).catch((err) => {
            console.error(err);
            reject(err);
          });
        }).catch((err) => {
          console.error(err);
          db.put({_id: key, expiresAt: expires, ...data}).then((status) => {
            resolve(status);
          }).catch(function(err){
            reject(err);
          });
        });
      }
      catch(error){
        console.error(error);
        reject(error);
      }
    });
  }

  _putEach(subkey, arr, expires) {
    const db = this.db;
    return new Promise((resolve, reject) => {
      if (!(arr instanceof Array)) {
        reject("Not instance of an array");
      }
      try {
        // const ids = _.uniq(arr.map(function(obj) {
        //   return obj.id
        // }));
        // ids = _.uniq(ids);

        arr = _.map(arr, (obj) => {
          const key = `${subkey}/${obj.id}`;
          db.get(key).then((doc) => {
            doc = {...doc, ...obj, expiresAt: expires};
            db.put(doc).then((status) => {
              return doc;
              // resolve(doc)
            }).catch((err) => {
              // reject(err)
              console.error(err);
            })
          }).catch((err) => {
            db.put({...obj, _id: key, expiresAt: expires}).then((status) => {
              return obj;
            }).catch((err) => {
              // reject(err);
              console.error(err);
            })
          })
        })
        resolve(arr);

        // db.bulkDocs(arr).then(function(l){
        //   console.log(l);
        //   resolve(l);
        // }).catch(function(err){
        //   console.error(err);
        //   reject(err);
        // })
      }
      catch(error){
        console.error(error);
        reject(error);
      }
    });
  }

  delete(key, isArr) {
    const db = this.db;
    return new Promise((resolve, reject) => {
      try {
        if (!isArr){
          db.get(key).then((doc) => {
            return db.remove(doc).then(() => {
              resolve(true);
            }).catch((err) => {
              reject(err);
            });
          }).catch((err) => {
            resolve(true);
          });
        } else {
          db.allDocs({
            include_docs: true,
            attachments: true,
            startkey: `${key}/`,
          }).then((arr) => {
            arr.rows.map((row) => {
              if (row.doc._id && row.doc._id.startsWith(key)){
                db.remove(row.doc._id, row.doc._rev).catch((err) => {console.error(err);});
              }
            });
            resolve(true);
          }).catch(function(err){
            console.error(err);
            reject(err);
          })
        }
      } catch(err) {
        reject(err);
      }
    });
  }


}
const DbClient = DbClient_;

export default DbClient;
