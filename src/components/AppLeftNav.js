import React, {PropTypes, Component} from 'react';
var Router = require('react-router');

import Colors from 'material-ui/lib/styles/colors';
import Spacing from 'material-ui/lib/styles/spacing';
import Typography from 'material-ui/lib/styles/typography';
const { MenuItem, LeftNav, Styles } = require('material-ui');
// const Colors = Styles.Colors;
// import tm from 'material-ui/lib/styles/theme-manager';

const ThemeManager = Styles.ThemeManager;
import NicoTheme from '../views/themes/NicoTheme';

class AppLeftNav extends Component {

  static propTypes = {
    history: PropTypes.object,
  }


  constructor() {
    super();
    this.toggle = this.toggle.bind(this);
    this._getSelectedIndex = this._getSelectedIndex.bind(this);
    this._onLeftNavChange = this._onLeftNavChange.bind(this);
    this._onHeaderClick = this._onHeaderClick.bind(this);
  }

  getStyles() {
    return {
      cursor: 'pointer',
      fontSize: '24px',
      color: Typography.textFullWhite,
      lineHeight: Spacing.desktopKeylineIncrement + 'px',
      fontWeight: Typography.fontWeightLight,
      // backgroundColor: ThemeManager.getCurrentTheme().palette.primary1Color,
      paddingLeft: Spacing.desktopGutter,
      paddingTop: '0px',
      marginBottom: '8px'
    };
  }

  render() {

    const {menuItems} = this.props;
    const header = (
      <div style={this.getStyles()} onClick={this._onHeaderClick}>
        GRAN LIGA
      </div>
    );

    return (
      <LeftNav 
        ref="leftNav"
        docked={false}
        isInitiallyOpen={false}
        header={header}
        menuItems={menuItems}
        selectedIndex={this._getSelectedIndex()}
        onChange={this._onLeftNavChange} />
    );
  }

  toggle() {
    this.refs.leftNav.toggle();
  }

  _getSelectedIndex() {
    const {menuItems} = this.props;
    let currentItem;
    for (let i = menuItems.length - 1; i >= 0; i--) {
      currentItem = menuItems[i];
      if (currentItem.route && this.props.history.isActive(currentItem.route)) return i;
    }
  }

  _onLeftNavChange(e, key, payload) {
    this.props.history.pushState(null, payload.route);
  }

  _onHeaderClick() {
    this.props.history.pushState(null, 'root');
    this.refs.leftNav.close();
  }


}

module.exports = AppLeftNav;