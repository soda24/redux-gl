import React, {Component, PropTypes} from 'react';
import FullWidthSection from './FullWidthSection';

import Paper from 'material-ui/lib/paper';
import FontIcon from 'material-ui/lib/font-icon';
import RaisedButton from 'material-ui/lib/raised-button';


export class Waiting extends Component {
  static propTypes = {
    className: PropTypes.string,
    message: PropTypes.string,
  }

  props = {
    className: ''
  }

  render() {
    const {message} = this.props; // eslint-disable-line no-shadow
    let {className} = this.props;
    className += ' btn btn-default';
    return (
      <div className={className}>
        {message}
      </div>
    );
  }
}


export class Err extends Component {
  static propTypes = {
    className: PropTypes.string,
    message: PropTypes.string,
  }

  props = {
    className: ''
  }

  render() {
    const {message} = this.props; // eslint-disable-line no-shadow
    let {className} = this.props;
    className += ' btn btn-default';
    return (
      <div className={className}>
        {message}
      </div>
    );
  }
}



export class RemoteError extends Component {
  static propTypes = {
    className: PropTypes.string,
    message: PropTypes.string,
  }

  props = {
    className: ''
  }

  render() {
    const {message} = this.props; // eslint-disable-line no-shadow
    let {className} = this.props;
    className += ' btn btn-default';
    return (
      <div className={className}>
        {message}
      </div>
    );
  }
}


export class FetchingFromServer extends React.Component {

    constructor(props, context) {
        super(props, context);
    }

    render() {
      return (
          <span>
                <div style={{
                  textAlign: "center",
                  top: "50%",
                    left: "50%",
                  position: "absolute",
                  transform: "translate(-50%, -50%)",
                }}>
                    <div style={{color: 'white'}}>
                        <FontIcon className="fa-refresh fa-spin" style={{fontSize: '8vmax',paddingLeft: '0.05vmax'}}/>
                        <div style={{marginTop: '20px'}}>Trayendo información del servidor</div>
                    </div>
                </div>
                <div className="clearfix" />
            </span>
      );
    }
}

export class ConnectionError extends React.Component {

    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <span>
                <div style={{
                    textAlign: "center",
                    top: "50%",
                    left: "50%",
                    position: "absolute",
                    transform: "translate(-50%, -50%)",
                }}>
                    <div>
                        <FontIcon className="fa fa-exclamation-triangle" style={{fontSize: '8vmax',paddingLeft: '0.05vmax'}}/>
                        <div style={{marginTop: '20px'}}>Problema contactando al servidor</div>
                    </div>
                </div>
                <div className="clearfix" />
            </span>
        );
    }
}


export class NoData extends React.Component {

    constructor(props, context) {
        super(props, context);
    }

    static propTypes = {
      backButton: PropTypes.node,
    }
    
    render() {
        return (
            <span>
                <div style={{
                    textAlign: "center",
                    top: "50%",
                    left: "50%",
                    position: "absolute",
                    transform: "translate(-50%, -50%)",
                }}>
                    <div>
                        <FontIcon className="fa fa-exclamation-triangle" style={{fontSize: '8vmax',paddingLeft: '0.05vmax'}}/>
                        <div style={{marginTop: '20px'}}>Falta Información</div>
                        {((btn) => { return this.props.backButton || <span />})()}
                    </div>
                </div>
                <div className="clearfix" />
            </span>
        );
    }
}


export class Loading extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            zDepth: 0
        }
    }

    render() {
        return (
            <span>
                <div style={{
                    textAlign: "center",
                    top: "50%",
                    left: "50%",
                    position: "absolute",
                    transform: "translate(-50%, -50%)",
                }}>
                    <div>
                        <FontIcon className="fa fa-refresh fa-spin" style={{fontSize: '8vmax', paddingLeft: '0.05vmax'}}/>
                        <div style={{marginTop: '20px'}}>Cargando información</div>
                    </div>
                </div>
                <div className="clearfix" />
            </span>
        );
    }
}

export class Expired extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            zDepth: 0
        }
    }

    render() {
      const {onClick} = this.props;
      return (
          <span>
              <div style={{
                  textAlign: "center",
                  top: "50%",
                  left: "50%",
                  position: "absolute",
                  transform: "translate(-50%, -50%)",
              }}>
                  <div>
                      <FontIcon className="fa fa-refresh" style={{fontSize: '8vmax',paddingLeft: '0.05vmax'}}/>
                      <div style={{marginTop: '20px'}}>
                        Información Expirada
                      </div>
                      <div style={{marginTop: '20px'}}>
                        <RaisedButton label="Actualizar" onTouchTap={(e) => {onClick();}}/>
                      </div>
                  </div>
              </div>
              <div className="clearfix" />
          </span>
      );
    }
}

export class NoInformationError extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            zDepth: 0
        }
    }

    render() {
      const {onClick} = this.props;
      return (
          <span>
              <div style={{
                  textAlign: "center",
                  top: "50%",
                  left: "50%",
                  position: "absolute",
                  transform: "translate(-50%, -50%)",
              }}>
                  <div>
                      <FontIcon className="fa fa-exclamation" style={{fontSize: '8vmax',paddingLeft: '0.05vmax'}}/>
                      <div style={{marginTop: '20px'}}>
                        No hay información
                      </div>
                      <div style={{marginTop: '20px'}}>
                        <RaisedButton label="Actualizar" onTouchTap={(e) => {onClick();}}/>
                      </div>
                  </div>
              </div>
              <div className="clearfix" />
          </span>
      );
    }
}

export class OnlineNeeded extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            zDepth: 0
        }
    }

    render() {
      const {onClick} = this.props;
      return (
          <span>
              <div style={{
                  textAlign: "center",
                  top: "50%",
                  left: "50%",
                  position: "absolute",
                  transform: "translate(-50%, -50%)",
              }}>
                  <div>
                      <FontIcon className="fa fa-refresh" style={{fontSize: '8vmax',paddingLeft: '0.05vmax'}}/>
                      <div style={{marginTop: '20px'}}>
                        Información Expirada
                      </div>
                      <div style={{marginTop: '20px'}}>
                        <RaisedButton label="Actualizar" onTouchTap={(e) => {onClick();}}/>
                      </div>
                  </div>
              </div>
              <div className="clearfix" />
          </span>
      );
    }
}

export class AuthNeeded extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            zDepth: 0
        }
    }

    render() {
      const {onClick} = this.props;
      return (
          <span>
              <div style={{
                  textAlign: "center",
                  top: "50%",
                  left: "50%",
                  position: "absolute",
                  transform: "translate(-50%, -50%)",
              }}>
                  <div>
                      <FontIcon className="fa fa-sign-in" style={{fontSize: '8vmax',paddingLeft: '0.05vmax'}}/>
                      <div style={{marginTop: '20px'}}>
                        Autorización requerida
                      </div>
                      <div style={{marginTop: '20px'}}>
                        <RaisedButton label="IR A LOGIN" onTouchTap={(e) => {onClick();}}/>
                      </div>
                  </div>
              </div>
              <div className="clearfix" />
          </span>
      );
    }
}
