import moment from 'moment'

export function expired(datestring) {
  return moment(datestring, 'YYYYMMDDhhmmdd') < moment();
}

export function guid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
    return v.toString(16);
  });
}

export function isStateReady(state) {
  const {
    cacheLoading,
    cacheLoaded,
    cacheError,
    remoteLoading,
    remoteLoaded,
    remoteError,
    cacheExpirationDate,
  } = state;
  // console.log('cacheExpirationDate', cacheExpirationDate);
  if (cacheLoading || remoteLoading) {
    return 'loading';
  }
  if (cacheError) {
    return 'error';
  }
  if (cacheExpirationDate && expired(cacheExpirationDate)) {
    return 'expired';
  }
  if (cacheLoaded) {
    return 'loaded';
  }
  else {
    return 'unknown';
  }
}
