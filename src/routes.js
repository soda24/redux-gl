import React from 'react';
import {Route, Redirect} from 'react-router';
import App from './views/App';
// import Widgets from './views/Widgets';
import Home from './views/Home';
import Tournaments from './views/Tournaments';
import TournamentsHoC from './views/TournamentsHoC';
import MatchHoC from './views/MatchHoC';
import Matches from './views/Matches';
import Match from './views/Match';
import Titulars from './views/MatchTitulars/Titulars';
import EditTitulars from './views/MatchTitulars/EditTitulars';
import EditMatchEvents from './views/MatchTitulars/EditMatchEvents';
import DebugMatchEvents from './views/MatchTitulars/DebugMatchEvents';
import Login from './views/Login';
import Logout from './views/Logout';
import NotFound from './views/NotFound';

export default (store) => {
  /**
   * Please keep routes in alphabetical order
   */
  return (
    <Route component={App}>
      <Route path="home" component={Home} />
      <Route component={TournamentsHoC} >
        <Route path="tournaments" component={Tournaments} />
        <Route path="tournaments/:tournamentId/matches" component={Matches} />
        <Route component={MatchHoC} >
          <Route path="tournaments/:tournamentId/matches/:matchId" component={Match} />
          <Route path="tournaments/:tournamentId/matches/:matchId/titulars" component={Titulars} />
          <Route path="tournaments/:tournamentId/matches/:matchId/titulars/:local" component={EditTitulars} />
          <Route path="tournaments/:tournamentId/matches/:matchId/events" component={EditMatchEvents} />
          <Route path="tournaments/:tournamentId/matches/:matchId/debug_events" component={DebugMatchEvents} />
        </Route>
      </Route>
      <Route path="/login" component={Login} />
      <Route path="/logout" component={Logout} />
      <Route path="*" component={Home} status={404} />
      <Redirect from='/' to='home' />
    </Route>
  );
};
